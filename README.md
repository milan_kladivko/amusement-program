
# Amusement Program


## Dev instructions

Clone this repo. 
```sh
git clone https://gitlab.com/milan_kladivko/amusement-program.git
cd ./amusement-program
```

Then for graphics and sound assets, either make a symlink 
to the dropbox folder (if you have it):
```sh
ln -s ~/Dropbox/Jii\ Projectsy/AmusementProgram/graphix ./
ln -s ~/Dropbox/Jii\ Projectsy/AmusementProgram/sfx ./
```

Or if you don't need the assets up to date immediatelly
and git pushes and pulls are fine for what you're doing, 
you can use the assets' git repo: 
```sh
git clone https://gitlab.com/milan_kladivko/amusement-program-assets.git
cd amusement-program-assets
./__rename_git_folder.sh
```

Open the project folder in Godot and it should be fine.


### Saving Aseprite spreadsheets

For some spritesheets, we'll want to get layers on one axis 
and frames on the other. Aseprite's UI doesn't have this option
but we can do it with the CLI: 

```sh
aseprite --batch  \
--list-layers --list-tags \
--split-layers --split-tags  \
--sheet-pack --trim  \
--sheet healthbar.png \
--data healthbar.json  \
healthbar.ase
```
> Note: The layers have to be listed manually for more complicated
> project files. Sometimes we also want a whole group to be exported 
> as a single layer in the sheet, so be ready for more complication.

This also exports a `.json` file that includes the metadata 
about frames', tags' and layers' positions. It saves us 
the hassle of clicking-in this info into the AnimationPlayer's GUI
or writing our own metadata to put it together. 

With `--trim`, the sheets' frames are also trimmed of any whitespace
and packed very closely together. With this, we can save spreadsheet
space but setting up the regions with Godot's GUI becomes pretty
much impossible. 

> Note: Trimming is weird when leaving empty frames  is wanted 
> and needed. Imports don't work very well with trimming ..for now!

The JSON parser, loader and Node creator is a bit complicated 
but it's still really worth it for any effective workflow. 

