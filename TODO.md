# Project's todo list

Most issues are on [the project's glo board](https://app.gitkraken.com/glo/board/XW9xa9aSlAAPAVeA),
but coding issues should preferrably be kept *here*. Keeping them so far from the code doesn't seem sane, among other workflow problems. 

## Instructions on how to keep the list tidy

1. __Keep these instructions up-to-date!__

1. __Use checkbox lists with `- [ ] Task`.__ 
   This isn't needed for heading-only tasks, but do use it 
   when we have many sub-tasks for a heading. 

1. __Annotate tasks with tags.__ 
   If possible, write tags at the beginning of the line.
   not at the end. Try to keep them look distinct at a glance (try different
   word lengths, case differences or special characters). 
   
   - Complete as soon as possible -- `[ASAP]`
   - Possible performance improvement -- `[perf]`
   - Chore / boring stuff -- `[chore]`
   - Nice to have / cosmetic / optional -- `[?]`
   - Bugs and errors -- `[bug]`
   - Bugs I can't fix right now -- `[oh_well]`
      
1. __Keep task priority with heading ordering.__
   That is, reorder the headings if we need to prioritize an entire task,
   beyond `[ASAP]` tasks. This is important for keeping work streamlined
   and to not get slowed down on unnecessary details. If possible, also keep
   the individual sub-task order too. Re-ordering completed tasks isn't
   needed.

# =====  TASKS BELOW  =====


# Console commands
- [x] screenshot tilemap by name
- [x] list all tilemaps in current scene
- [ ] list tilemaps that player's currently in


# Console functionality
### Command types 
- [X] single command, no arguments
- [X] multiple, no arguments
- [ ] multiple, with arguments
  - [x] strings (easy)
  - [ ] all the other ones
### General
- [X] colored output
- [ ] `[ASAP]` transparent mode for runtime debugging
- [ ] better (mono?) font
- [ ] command history (up arrow)
- [ ] command hints and autocompletion (tabbing)


# Advanced_Tilemap 
Extend the TileMap for placeholder nodes and submapping so I can draw in complex nodes and tilemaps with different physics. 

### Features
- [X] Rotating and axis flipping of placeholder objects
- [ ] Allow more options for arguments in the name

### Screenshotting for art
- [x] Camera and window control
- [ ] `[bug]` Window stretch settings
- [ ] `[?]` Make toggle-able - back and forth 


# Make use of the *AsepriteJSON* in particles


# Level component
- [ ] Level scene loader 


# Player code decoupling and refactoring
- [x] Camera
- [x] Advanced tracking booleans
- [x] Hitboxes
- [ ] Wall hitboxes too tho!
- [ ] Particles and FX
- [ ] ?? Combat, checkpoints and health ??
- [ ] Animations
- [ ] Sounds


# Logging into a file


# `[bug]` Frame perfect jumps don't reset jumping animation
Probably an issue with the input buffer + order of animation
handling and jumping. 


# Tweak the camera easing a bit


# Sway the HUD with camera movements? 


# `[?]` Side scrolling style camera
- [ ] Area type
- [ ] Camera behavior


# `[?]` Resolve @todo in *Camera.gd*


# Add walljump / wallslide smoke 


# Arrow traps

### Functionality
- [ ] `[perf]` Improve timer handling 
- [x] Placeholder integration in *Advanced_TileMap*

### Cosmetic Effects
- [ ] smoke on shoot
- [ ] crash / delete effect


# Physics-based debris


# Respawning effects


