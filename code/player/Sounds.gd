extends Node2D
class_name Player_Sounds

onready var sfx = {
	'landing': load('res://sfx/landing.wav')
}

onready var steps = $steps


func _ready(): 
	steps.stream_paused = true


func landing(): 
	steps.volume_db = -16
	steps.stream = sfx.landing
	steps.pitch_scale = Rand.range(0.8, 1.0)
	steps.play()
	
	
func jump(): 
	steps.volume_db = -20
	steps.stream = sfx.landing
	steps.pitch_scale = Rand.range(1.4, 1.8)
	steps.play()
	
