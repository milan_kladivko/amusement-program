#===============================================================================
# 
#  Here lies the game's main character. This is the most expansive piece 
#  of code in the game, as should be expected for a platformer. 
#  
#  Timing floats are always in seconds. 
#  
#  Floats like `resistance` are almost always factors that multiply
#  the value in question. 
# 
#  The character was one of the first things written into the code
#  so some code might be finicky, badly written and generally outdated
#  and shameful. Proceed with caution. 
# 
#  Decoupling the code here is very tough as everything is interconnected. 
#  Jumping means you can't slide, you can't trigger some animations, 
#  some timers are reset, you get the idea. It takes a lot of thought 
#  to make it readable... so I don't really try as much as I should. 
#  
#===============================================================================

extends KinematicBody2D
class_name Player


# Game's basic physics
onready var GRAVITY_GLOBAL = __.get_physics_gravity(self)
onready var UP_DIRECTION = GRAVITY_GLOBAL * -1

# Movement speeds
const MAX_RUN_SPEED = 240
const RUN_ACCEL = 0.050 * MAX_RUN_SPEED 
const FALL_SPEED_LIMIT = 500

# Friction
const FLOOR_RESISTANCE = 0.60
const SLIDE_RESISTANCE = 0.98
const AIR_RESISTANCE = 0.90
const WALL_RESISTANCE = 0.90

# Jump strengths
const JUMP_IMPULSE = -260
const WALLJUMP_Y_IMPULSE = JUMP_IMPULSE * 1.2
const WALLJUMP_X_IMPULSE = MAX_RUN_SPEED * 0.8
const JUMP_INBUFFER = 4 * 1.0/60

const SLIDE_X_IMPULSE = MAX_RUN_SPEED * 1.5
const SLIDE_MAX_TIME = 1.2
const SLIDE_MIN_TIME = 0.35
const SLIDE_COOLDOWN = 0.3

# Counters for jumping mechanics
const JUMP_MIN_TIME = 0.08  # the smallest press jumps one tile high


#------------------------------------------------------------------------
#   Caching Nodes  ...consider moving to respective parts of code
#------------------------------------------------------------------------
onready var hitbox : CollisionPolygon2D = $Walls_Hitbox
onready var hitbox_extents = __.polystats_of( hitbox.polygon )
onready var hitbox_slide : CollisionPolygon2D = $Slide_Walls_Hitbox

onready var Detection : Node2D = $Detection

onready var Wall_Checks := $Wall_Jumping  # @todo; move to Detection

onready var sprite : Sprite = $Sprite
onready var anim : AnimationPlayer = $AnimationPlayer
onready var scaler : AnimationPlayer = $SpriteScaler

onready var att_areas : Attack_Areas = $Attack_Areas
onready var att_slash1 := $Attack_Areas/slash_1
onready var att_slash2 := $Attack_Areas/slash_2

onready var sfx := $Sounds

onready var cam : Camera2D = $Use_Global_Center/Camera
onready var hud : Node2D = cam.get_node('HUD')



#===============================================================================
#  Node constructor code
#===============================================================================
func _ready(): 
	
	# from this point, nodes can use global getter to get the player
	__.register_player_globally( self )

	Wall_Checks.init()
	Detection.init() 
	
	init_healthbar_graphics()
	
	add_dev_console_commands()
	
	
#===============================================================================
#  Game progress variables
#===============================================================================

var pickups = 0
var WALLJUMP = 1 << 1
var SLIDE = 1 << 2
var WEAPON = 1 << 3

func get_walljump(): 
	pickups = pickups | WALLJUMP
func get_slide(): 
	pickups = pickups | SLIDE
func get_weapon(): 
	pickups = pickups | WEAPON

# Checkpoint to return to
var last_checkpoint = null	

func teleport_to_last_checkpoint(): 
	self.velocity = Vector2.ZERO
	if last_checkpoint != null: 
		self.position = last_checkpoint.global_position
	else: 
		self.position = Vector2.ZERO


#===============================================================================
#  User Input and Controls
#===============================================================================

# handle keys in their own file
var keys = preload('Controls.gd').new()  

func _unhandled_input(event):
	# Send events for input buffering, if applicable
	keys.buffer_action_from(event) 


	
#===============================================================================
#  Physics-state, world and movement variables and detection
#===============================================================================

const Tracking_Group = preload('res://code/utils/Tracking_Group.gd')

# 
#  List of player's detection variables
# 
var bools = Tracking_Group.new([
	# floors
	'on_floor', 'running', 'running_counter', 'braking', 'sliding', 
	# airs
	'in_air', 'rising', 'falling', 'jumping', 
	# walls
	'walljumping', 'near_wall', 'near_left_wall', 'near_right_wall', 
	# other
	'dying'
])


# Special movement vars
var jump_cut_pending :bool = false    # interrupt jump asap (JUMP_MIN_TIME)
var slide_remaining :float = 0

# Character's velocity applied every physics tick
var velocity := Vector2.ZERO

# Current physics tick's delta in seconds
# Exposes the frame-time outside the gameloop's scope
var current_delta :float = 0 


	

func update_detection():
	
	# Rising and falling
	_switch('rising', (velocity.y < 0 and _is('in_air')))
	_switch('falling', (velocity.y > 0 and _is('in_air'))) 
	
	# Ignore thin platforms when rising
	Detection.ignore_thin_platforms_if( _is('rising') )
	
	# in Air / on Floor
	### var builtin_floor_det = is_on_floor()  # buggy as all hell
	_switch('on_floor', \
			Detection.is_on_floor() )
	if _started('on_floor'): 
		sfx.landing()
		make_land_particles(1)
		
	_switch('in_air', _is_not('on_floor'))
	if _started('walljumping'): 
		_switch('in_air', true, 'and reset')

	# Wall detection
	_switch('near_left_wall', Wall_Checks.touching_wall_left())
	_switch('near_right_wall', Wall_Checks.touching_wall_right())
	if _is('near_left_wall') or _is('near_right_wall'): 
		_switch('near_wall', true)
		if _started('near_wall') and _is('in_air'): 
			sfx.landing()
	else:
		_switch('near_wall', false)
	

#-------------------------------------------------------------------
#   Attacks and damage behaviour
#-------------------------------------------------------------------

var FULL_HEALTH :int = 5
var health_points := FULL_HEALTH

var current_attack : String = ''
var current_attack_remaining : float = -1

var is_immune_to_damage :bool = false
var DAMAGE_IMMUNE_DURATION :float = 0.5

onready var healthbar : Node2D = hud.get_node('Hp_Bar')


func update_healthbar(initial:int, final:int, force_change:bool=false) -> void: 
	# range of change
	var low  :int = min(initial, final)  
	var high :int = max(initial, final)
	
	for i in [1,2,3,4,5]: 
		var is_changed :bool = (i >= low) and (i <= high) 
		if is_changed or force_change: 
		
			# @performance; cache the pegs into an array (no string lookups)
			var peg :Sprite = healthbar.get_node( str(i) )   # raunchy! 
			var anim :AnimationPlayer = peg.get_node('AnimationPlayer')
			
			var is_on  :bool = (i <= final)
			var is_off :bool = (i >  final)
		
			if is_on: 
				anim.play('idle')
			elif is_off:
				anim.play('remove')
	
	
func change_health_points( change_by :int = 0 ) -> void: 
	
	Console.warn( 'HP ' + str(change_by) )
	
	var force_update_all = (change_by == 0)
	update_healthbar( health_points, 
					  health_points + change_by, 
					  force_update_all )
	
	health_points += change_by
	if health_points <= 0: 
		wait_and_die()
	
	Console.warn( ' -> ' + str(health_points) +'\n')
	
	
func full_heal() -> void: 
	var added_health :int = FULL_HEALTH - health_points
	change_health_points( + added_health )
	

func init_healthbar_graphics() -> void: 
	var meta_filepath := 'res://graphix/UI/healthbar.json'
	var handler := Animation_Utils.AsepriteJSON
	var sprites := handler.to_animated_sprites( meta_filepath )
	for peg in sprites: 
		healthbar.add_child( peg )
	
	var ONLY_INIT = 0
	change_health_points( ONLY_INIT )
		
		
func take_damage_from( who: Node ) -> void: 
	var DEFAULT_DAMAGE = 1
	var damage = DEFAULT_DAMAGE
	if who.get('do_damage') != null: 
		damage = who.do_damage
	change_health_points( -damage )
		
		
func wait_and_die(): 
	if _is('dying'): 
		return
	
	_switch('dying', true)
	keys.block_keys = funcref(keys, 'block_all_player')
	velocity = 0.3 * Vector2(MAX_RUN_SPEED * get_x_scale() * -1, 
					   JUMP_IMPULSE) 
	
	var WAIT_TIME = 0.6
	yield(get_tree().create_timer( WAIT_TIME ), 
		  'timeout')
	
	keys.block_keys = null
	teleport_to_last_checkpoint()
	_switch('dying', false)
		

func _on_Damage_Detector_body_entered( dmg_dealer ) -> void:
	
	if dmg_dealer.is_in_group('dmg_instakill'): 
		Console.err('instakill!')
		change_health_points( -9999 )
		Console.err('is here!')
			
	elif dmg_dealer.is_in_group('dmg_partial'): 
		get_hit_by(dmg_dealer)
	
		
func get_hit_by(attack_node:Node2D): 
	
	var attacker := __.find_first_scripted_ancestor_node( attack_node )
	
	take_damage_from(attacker)
		
	# Let the attacker know he hit successfully
	if attacker.has_method('do_collision_action'): 
		attacker.do_collision_action()
		
	# Shake the screen a bit
	cam.screen_shaker.start_new( 4, 0.4 )
	
	# Make a hit particle
	var part:Node2D = Particle_System.spawn_new_particle('get_slashed')
	part.rotation = Rand.range(0, 2*PI)
	part.scale = Vector2(0.5, 0.5)
	## Get position of hit: 
	## https://docs.godotengine.org/en/3.1/classes/class_geometry.html#class-geometry-method-line-intersects-line-2d
	var coll_point = (attacker.global_position + self.global_position) / 2
	part.position = coll_point
	Particle_System.play_particle(part)
	

	
func apply_attacks(): 

	if current_attack_remaining < 0:
		
		current_attack = ''
		
		if keys.pressed('player_attack'): 
			
			if _is('on_floor'): 
				current_attack = 'slash_1'
			
				# animation
				anim.play('slash_1')
				current_attack_remaining = anim.current_animation_length
				# hitbox
				att_areas.enable_attack_area(att_slash1)
				# shove the player forward a bit
				velocity.x = 300 * sign(att_areas.scale.x)
				velocity.y = -50
				
			elif _is('in_air'): 
				current_attack = 'airslam'
				
				# animation
				anim.play('slash_1')
				current_attack_remaining = 1
				#hitbox
				att_areas.enable_attack_area(att_slash1)
				velocity.x = 100 * sign(att_areas.scale.x)
				velocity.y = 300
	
	else: 
		if current_attack == 'airslam' and  \
		   (_started('on_floor') or _started('near_wall')): 
			
			current_attack_remaining = 0
		else: 
			current_attack_remaining -= current_delta
	
			
#----------------------------------------------------------------------------
#  Movement and physics interaction functions
#----------------------------------------------------------------------------

func apply_gravity():
	
	var gravity_pull = GRAVITY_GLOBAL
	# if is_near_wall and _is('rising'):  gravity_pull *= 0.5

	# Always do gravity
	# @note; thin platforms will null v.y of there's no grav
	velocity += gravity_pull
	
	# halt movement when stuck on wall
	if (pickups & WALLJUMP): 
		if _started('near_wall') and not _started('walljumping'): 
			velocity.y = min(50, velocity.y)
		
	# apply wall resistance when on walls
	if _is('near_wall') and _is('falling') and (pickups & WALLJUMP): 
		velocity.y *= WALL_RESISTANCE
		
	# limit falling speed
	velocity.y = min(velocity.y, FALL_SPEED_LIMIT)
	



func apply_running():
	
	var run = 0
	var current_speed = velocity.x
	
	# Movement signalled by player left and right controls
	# Ignore run inputs when attacking and sliding
	if current_attack == '' and _is_not('sliding'): 
	
		# Set the run acceleration
		if keys.pressing("player_left"):
			run -= RUN_ACCEL
		elif keys.pressing("player_right"):
			run += RUN_ACCEL
	
		
	
	var accelerating = (run != 0)  # player pressed a button
	if accelerating: 
		
		_switch('running', true)
		_switch('braking', false)
		
		var resisting_current_movement = sign(current_speed) != sign(run)
		if not resisting_current_movement:
			
			_switch('running_counter', false)
			
			var above_speed_limit = abs(current_speed) > MAX_RUN_SPEED 
			if not above_speed_limit:
				velocity.x = clamp(current_speed + run,  \
								   -MAX_RUN_SPEED, +MAX_RUN_SPEED)
				
			elif above_speed_limit and _is('on_floor'):
				velocity.x = clamp(current_speed + run,  \
								   -MAX_RUN_SPEED, +MAX_RUN_SPEED)
				
				# ease into the limit a bit
				var faster_by = current_speed - (  \
									MAX_RUN_SPEED * sign(current_speed))
				velocity.x += round(faster_by * 0.8)
				
			else: 
				pass  # Don't limit the speed in air
				
		elif resisting_current_movement:
			
			_switch('running_counter', true)
			
			if _is_not('walljumping'):
				# Stop the current speed sooner, but not if walljumping
				velocity.x *= FLOOR_RESISTANCE  # Apply a stop-resistance
			
			# Apply a running speed counter to the movement
			velocity.x += run
			
	elif not accelerating:
		
		_switch('running', false)
		_switch('braking', true)
		
		if _started('on_floor'): 
			# Reduce the speed a lot if player landed without pressing anything
			velocity.x *= 0.4
		
		if _is('on_floor'):
			if _is_not('sliding'): 
				velocity.x *= FLOOR_RESISTANCE
			else: 
				velocity.x *= SLIDE_RESISTANCE
			
		elif _is('in_air'):
			velocity.x *= AIR_RESISTANCE
			


func apply_jumping():

	# Floor resets the jump
	if _is('on_floor'): 
		_switch('jumping', false)
		jump_cut_pending = false
		_switch('walljumping', false)
	
	# @note; slide jumps are handled in slide
	if keys.pressed("player_jump", JUMP_INBUFFER) and _is_not('sliding'):
		
		# Regular floor jump
		if _is('on_floor'):
			_switch('jumping', true)
			
			velocity.y = JUMP_IMPULSE
			
			sfx.jump()
			make_land_particles(3, 2)
			
		# Wall jump
		elif _is('near_wall') and (pickups & WALLJUMP):
			
			# @note/ this line lets walljumps be jumpcut without limit
			# _switch('jumping', true)  
			
			_switch('jumping', true, 'with reset')
			_switch('walljumping', true, 'with reset')
			
			var y_imp = WALLJUMP_Y_IMPULSE
			var x_imp = WALLJUMP_X_IMPULSE
			
			if _is('near_left_wall'):
				velocity.x = +x_imp
			if _is('near_right_wall'):
				velocity.x = -x_imp
			velocity.y = y_imp
			
			sfx.jump()
			

	# Cutting the jump earlier (lowering)
	if not jump_cut_pending: 
		var FEW_FRAMES = 0.03  # release can happen before rise 
		if keys.released('player_jump') and _is('jumping') and  \
		   (_is('rising') or  \
			_secs_since_changed('jumping') < FEW_FRAMES):
	
			if _secs_since_changed('jumping') > JUMP_MIN_TIME:
				velocity.y = JUMP_IMPULSE * 0.2
			else: 
				jump_cut_pending = true  # do it asap  @@
			
	if jump_cut_pending and _is('rising') and  \
	   _secs_since_changed('jumping') > JUMP_MIN_TIME:
		
		# and doing it now  @@
		velocity.y = JUMP_IMPULSE * 0.2
		jump_cut_pending = false
		
	
	# Sliding along the wall detection
	var smoke_amount_by_vel = abs(velocity.y * 2 / MAX_RUN_SPEED)
	if _is('near_wall') and _is('in_air'):
		# @todo; spawn wall slide smoke or something
		pass

		
	
func apply_sliding(): 
	
	# Decrease slide timer (positive = sliding // negative = cooling_down)
	slide_remaining -= current_delta		
	
	# Start sliding
	if _is_not('sliding') and _is('on_floor') and _is_not('near_wall'): 
		
		var slide_cooled_down = slide_remaining < -SLIDE_COOLDOWN
		
		var slide_L = (keys.pressing('player_left')  and  \
					   _is_not('near_left_wall'))
		var slide_R = (keys.pressing('player_right') and  \
					   _is_not('near_right_wall'))
		var commanded_slide = keys.pressing('player_down') and  \
							  (slide_L or slide_R)
		
		if commanded_slide and slide_cooled_down: 
		
			_switch('sliding', true)
			Detection.enable_slide()
			slide_remaining = SLIDE_MAX_TIME
	
			var speed = SLIDE_X_IMPULSE  # Faster than runspeed
			if keys.pressing('player_left'): 
				velocity.x = -speed
				hitbox_slide.scale.x = -1
			elif keys.pressing('player_right'):
				velocity.x = +speed
				hitbox_slide.scale.x = +1
		
	# While sliding... 
	if _is('sliding'): 
		
		# Ensure a minimal speed 
		# ...when you can't stand up for too long (long holes)
		var MIN_SLIDE_SPEED = MAX_RUN_SPEED * 0.3
		velocity.x = sign(velocity.x) * max(abs(velocity.x),  \
											MIN_SLIDE_SPEED)

	
		# Slide ends?
		var user_cancelled = not keys.pressing('player_down') or  \
							 keys.pressed('player_jump')
							 
		var max_slide_remaining = (SLIDE_MAX_TIME - SLIDE_MIN_TIME)
		var can_be_cancelled = slide_remaining < max_slide_remaining
	
		var is_not_moving = abs(velocity.x) < 0.2
		var interrupted = _is('in_air') or is_not_moving
		
		if slide_remaining <= 0 or  \
		   interrupted or  \
		   (can_be_cancelled and user_cancelled): 
		
			if Detection.can_stand_up(): 
			
				# Turn off slide
				_switch('sliding', false)
				slide_remaining = 0  
				Detection.enable_stand()
				
				# Jump out of slide! 
				if keys.pressed('player_jump'):
					# do a slide jump, only if character can stand up
					_switch('jumping', true)
					# don't allow funky speedboost stuff
					velocity.x = clamp(velocity.x,  \
									   -MAX_RUN_SPEED, +MAX_RUN_SPEED)
					velocity.y = JUMP_IMPULSE
			


#===============================================================================
#  Direction stuff
#===============================================================================
func set_look_left(look_left:bool) -> void: 
	var x_scale = look_to_x_scale( look_left )
	sprite.flip_h = look_left 
	Detection.scale.x = x_scale
	att_areas.scale.x = x_scale
	
func get_look_left() -> bool: 
	var look_left = sprite.flip_h
	return look_left

func get_x_scale() -> int: 
	return look_to_x_scale( get_look_left() )

func look_to_x_scale(look_left:bool) -> int: 
	var x_scale = -1 if look_left else 1   # look > int sign
	return x_scale


func turn_around_if_needed():
	 
	# Keep current flip by default 
	var look_left = get_look_left()
	
	# Change based on speed
	if velocity.x > 0: 
		look_left = false
	elif velocity.x < 0: 
		look_left = true
	
	# Flip the pic if running in place (like against a wall) 
	if _is('running') and velocity.x == 0: 
		if keys.pressing('player_right'): 
			look_left = false
		elif keys.pressing('player_left'):
			look_left = true
		
	# When wallsliding
	if (pickups & WALLJUMP) and _is('in_air'): 
		if _is('near_left_wall'): 
			look_left = false
		elif _is('near_right_wall'): 
			look_left = true

	set_look_left( look_left )
	

#-------------------------------------------------------------------------------
#   Particles and effects
#-------------------------------------------------------------------------------
	
onready var run_part_names = Particle_System.collect_matching_particle_names('run_*')
onready var simple_part_names = Particle_System.collect_matching_particle_names('simple_*')

func make_step_particle(): 
	var fx_name = Rand.item(run_part_names)
	var particle = Particle_System.spawn_new_particle(fx_name, null)
	particle.position = $Effects/SmokePoints/run.position + global_position
	particle.scale = Vector2( get_x_scale(), 1 )
	Particle_System.play_particle(particle)
	
	
func make_land_particles(n:int=1, radius:float=1): 
	for i in n: 
		var fx_name = Rand.item(simple_part_names)
		var particle = Particle_System.spawn_new_particle(fx_name, null)
		
		particle.scale = Vector2( get_x_scale(), 1 )
		particle.position = $Effects/SmokePoints/land.position + global_position
		particle.position += Rand.offset_radius(radius)
		Particle_System.play_particle(particle)		
		

func update_animation():

	turn_around_if_needed()
	
	if _is('dying'): 
		scaler.play('dying')
		return
	else: 
		sprite.rotation_degrees = 0
	
	# 
	#  Minor speed-up of idle animation for the character to appear winded
	# 
	var anim_speeder :AnimationPlayer = $Effects/Speeder
	
	if _is('on_floor') and _is_not('sliding') and  \
	   (_stopped('running') or _stopped('sliding') or  \
		(_is_not('running') and _stopped('in_air') )): 
		
		anim_speeder.stop( true )
		var secs :float = 2
		anim_speeder.playback_speed = 1 / secs
		anim_speeder.play('2x speed ease') 
		
	if _started('sliding'): 
		anim_speeder.stop( true )
		anim_speeder.playback_speed = 1 / SLIDE_MAX_TIME
		anim_speeder.play('1.5x speed ease')
		
	if _started('in_air') or _started('running'): 
		anim_speeder.stop( true )
		anim_speeder.play('DEFAULT')
	
	
	
	# 
	#  Sprite stretching
	#
	
	if keys.pressed('player_down') and _is('on_floor') and _is_not('running'): 
		scaler.play('landed_shrink')
	
	if _is('falling') and _is_not('near_wall'):
		scaler.play('falling')
	if _is('on_floor') and scaler.current_animation == 'falling': 
		scaler.play('landed_shrink')  # maybe use a resetter animation??
		
	if _started('on_floor'): 
		scaler.play('landed_shrink')
	
	if _started('jumping'): 
		scaler.play('jumped_stretch')
		
	if _started('walljumping'): 
		if sprite.flip_h: 
			scaler.play('wall_right_jump')
		else: 
			scaler.play("wall_left_jump")
	
	if (pickups & WALLJUMP): 
		if (_started('near_wall') and _is('in_air') or \
			# might not be needed: 
			(_is('near_wall') and scaler.current_animation == 'falling')): 
			
			if sprite.flip_h: 
				scaler.play('wall_right_landed')
			else: 
				scaler.play("wall_left_landed")
	
	
	# 
	#  Sprite Animations
	# 
			
	var now_playing = anim.current_animation
	if current_attack == '': 
		
		var a = null  # final animation var
		
		if _is('on_floor'):
			if _is('running'): 
				a = 'run'
			elif _is_not('running') and _is_not('sliding') and  \
				 keys.pressing('player_down'):
				
				a = 'crouch'
			else: 
				a = 'idle'
		
		if _started('jumping') and abs(velocity.x) < (MAX_RUN_SPEED/2): 
			a = 'jump_up'
		if _is('in_air') and now_playing != 'jump_up': 
			a = 'air'
		
		if _is('near_wall') and _is('in_air') and not _now_changed('walljumping') and (pickups & WALLJUMP): 
		   	a = 'wall_cling'
			
		
		if _is('sliding'): 
			
			if now_playing != 'slide_start': 
				a = 'slide_loop'
				
			else: 
				a = null  # cancel anything interrupting slide starting
				
			if _started('sliding'): 
				a = 'slide_start'
				scaler.play('started_sliding')
				
		if _stopped('sliding') and now_playing != 'slide_start' and  \
		   _is_not('jumping'): 
			
			a = 'slide_end'
			scaler.play('reset')
			
		if now_playing == 'slide_end' and  \
		   _is_not('jumping') and  \
		   (a != 'crouch'): 
			
			a = null
			
		# finally play animation 
		if a != null:
			anim.play(a)
		

			
# --------------------------------------------------------------------
#   Game Loop
# --------------------------------------------------------------------


# Physics tick (fixed-step, constant 60fps)
func _physics_process(delta):	
	
	# Allow the whole class to see this step's delta time
	current_delta = delta  
	bools.step_all_by( current_delta )
	
	update_detection()
	apply_attacks()
	apply_running()
	apply_jumping()
	apply_sliding()
	apply_gravity()
	
	update_animation()
	
	# Apply the calculated velocity for next move
	var leftover_velocity:Vector2 = move(velocity)
	
	# Finally update velocity to one that collided
	velocity = leftover_velocity


# Frame tick 
# (don't use for things dependent on things called in _physics_process,
#  it changes based on performance)
func _process(delta):
	pass


func move(by_velocity:Vector2) -> Vector2:
	
	# This probably doesn't really matter. The tutorials
	# about using `move_and_slide()` are so sparse
	# that I don't trust any of these to be stable.
	
	var stop_on_slopes = true  # Prevent idle sliding?? 
	var max_slides = 4  # Max detected collisions?? 
	
	# Make sure the 45deg slopes are really floors
	# Is used when calling the builtin wall detection
	#   ...not used here, of course
	var floor_max_angle = PI * 0.252  
	
	var leftover_vel = move_and_slide(
		by_velocity, UP_DIRECTION, 
		stop_on_slopes, max_slides, floor_max_angle)
	
	return leftover_vel



#-------------------------------------------------------------------------------
# Aliases for `Tracking_Group` functions
#  @note/  Because we don't have `using` keyword in GDScript, 
#          gotta use these funky aliases in any scene using this 
#          class often. Sucks. 

func _switch(bool_name:String, value:bool, force_change:=''): 
	return bools.switch_( bool_name, value, force_change )

func _is(bool_name:String) -> bool: 
	return bools.is_( bool_name )

func _is_not(bool_name:String) -> bool: 
	return bools.is_not_( bool_name )

func _started(bool_name:String) -> bool: 
	return bools.started_( bool_name )

func _stopped(bool_name:String) -> bool: 
	return bools.stopped_( bool_name )
	
func _now_changed(bool_name:String) -> bool: 
	return bools.now_changed_( bool_name )

func _secs_since_changed(bool_name:String) -> float: 
	return bools.secs_since_changed_( bool_name )


#------------------------------------------------------------------------------
#  Some player-specific console commands
# 
func add_dev_console_commands() -> void: 

	Console.register_command(
		'backlight', 
		"Toggles the player's backlight effect", 
		[], self, 'toggle_backlight')

	Console.register_command(
		'get_walljump', 
		"Enable the walljump", 
		[], self, 'get_walljump')
	
	Console.register_command(
		'slomo', 
		'Slow Motion!', 
		[], self, 'halfspeed')
	
func halfspeed() -> void: 
	Engine.set_time_scale( 0.5 )

func toggle_backlight() -> void: 
	var light:Light2D = $Effects/Backlight
	light.visible = not light.visible

