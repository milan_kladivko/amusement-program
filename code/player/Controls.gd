extends Object

#------------------------------------------------------------
#  User input, controls and actions
#------------------------------------------------------------  

# Controls list
onready var actions = null
# List of actions that can be buffered 
onready var buffered_actions = null
# Timestamps of last keypresses of actions
var pressed_timestamps : Dictionary = {}

func _init():  # called on .new()
	actions = InputMap.get_actions()
	buffered_actions = make_buffered_action_list()



func make_buffered_action_list(): 
	var buffered_action_matchstr = 'player_*'
	var collected_actions = []
	for act in actions: 
		if act.match(buffered_action_matchstr): 
			collected_actions.append(act)
	return collected_actions


func buffer_action_from(event): 
	""" 
	Builds a list of actions to be bufferable and makes timestamps
	of actions when they happen. These timestamps can be then 
	compared with the current timestamp to allow late keypresses.

	@note; beware timestamp overflows!!  ...well, maybe.
	"""
	
	# ignore non-gameplay events
	if !( typeof(event) in [typeof(InputEventKey), 
							typeof(InputEventJoypadButton), 
							typeof(InputEventJoypadMotion)] ): 
		return false
	
	# Find current event's action in the list of buffered ones
	var action = ''
	for act in buffered_actions: 
		if event.is_action(act):
			action = act
			# don't check for others after finding exact match
			break  
	var buffer_this_action = action != ''
	if not buffer_this_action: return false
	
	var just_released = event.is_action_released(action)
	if just_released: 
		# get rid of timestamp on release
		pressed_timestamps[action] = 0 
		return  # skip the rest, button isn't pressed anyway
		
	# ignore text-like key repeats
	var just_pressed = false
	if not event.is_echo():
		if action in pressed_timestamps:
			just_pressed = pressed_timestamps[action] == 0
		else: 
			just_pressed = true
		
	# update action's timestamp, only on start press
	if just_pressed:  
		pressed_timestamps[action] = OS.get_ticks_msec()


func check_action_buffer(key, buffer_secs):
	""" Checks an action buffer for recent pressed keys """
	var NULL_BUFFER_ON_SUCCESS = true
	if buffer_secs <= 0: 
		return false
	var buffer_msecs = buffer_secs * 1000.0
	if key in pressed_timestamps: 
		var diff = OS.get_ticks_msec() - pressed_timestamps[key] 
		if diff < buffer_msecs:
			if NULL_BUFFER_ON_SUCCESS: pressed_timestamps[key] = 0
			return true
	return false


var block_keys : FuncRef = null  # funcref(self, 'block_all_player')
func should_key_be_blocked(key): 
	var is_blocked = (block_keys != null and block_keys.call_func(key))
	var is_console_open = Console.is_shown
	return (is_blocked || is_console_open)
	

# Key blocking functions 
func block_all_player(key): 
	return key.begins_with('player_')





# Proxies for detecting user controls
func pressed(key, buffer_secs=0.0):
	if should_key_be_blocked(key): 
		return false
	var just_pressed = Input.is_action_just_pressed(key)
	var key_was_buffered = check_action_buffer(key, buffer_secs)
	return just_pressed or key_was_buffered

func released(key):
	var just_released = Input.is_action_just_released(key)
	return just_released

func pressing(key, buffer_secs=0.0):
	if should_key_be_blocked(key): 
		return false
	# Use buffer if key should be detected even when released too soon
	var being_pressed = Input.is_action_pressed(key)
	var key_was_buffered = check_action_buffer(key, buffer_secs)
	return being_pressed or key_was_buffered

