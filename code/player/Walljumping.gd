extends Node2D


const RAY_COUNT_SIDE = 5
const RAY_PERC_PASSES = 0.3

const RAYS_DEBUG_VISIBLE = false

var raycasts = {'left': [], 'right': []}
var player = null


	
func touching_wall_left() -> bool: 
	return check_if_wall(raycasts['left'])

func touching_wall_right() -> bool: 
	return check_if_wall(raycasts['right'])

func init(): 
	self.player = __.get_global_player()
	make_wall_raycasts()

	

func make_wall_raycasts(n:int=RAY_COUNT_SIDE): 

	var ex = player.hitbox_extents
	var allowance = ceil(ex['max_x'] * 0.5)
	var cast = Vector2(ex['max_x'] + allowance, 0)
	
	var height = ex['height']
	var min_y = ex['min_y']
	
	for side in raycasts.keys(): 
		for i in range(n): 
			
			var ray = RayCast2D.new()
			
			ray.enabled = true
			ray.position.y = (height / n) * (i + 0.5) + min_y
			ray.cast_to = (+cast) if side == 'right' else (-cast)
			ray.name = 'wallray_' + side + '_' + str(i)
			ray.visible = RAYS_DEBUG_VISIBLE
			
			ray.collision_mask = 0
			ray.set_collision_mask_bit(__.LAYER_WALL, true)
			
			self.add_child(ray)
			raycasts[side].append(ray)



func check_if_wall(wall_raycasts : Array) -> bool: 	
	
	var wall_collision_counter : float = 0
	
	for ray in wall_raycasts: 
		if not ray.is_colliding():
			continue  # skip this ray
		
		var collider : Object = ray.get_collider() 
		if typeof(collider) != typeof(TileSet): 
			# this couldn't be a thin_platform, count as success
			wall_collision_counter += 1
			continue
				
		var point : Vector2 = ray.get_collision_point()
		var further : Vector2 = -5 * ray.get_collision_normal()
				
		var tileset : TileSet = collider
		var pos : Vector2 = tileset.world_to_map(point + further)
		var tile_id : int = tileset.get_cellv(pos)
		var tile_name : String = tileset.tile_set.tile_get_name(tile_id)
		if tile_name != 'thin_platform':
			# anything but thin_platform's are good as walls
			wall_collision_counter += 1

	var perc_passed = wall_collision_counter / len(wall_raycasts)
	return (perc_passed >= RAY_PERC_PASSES)
