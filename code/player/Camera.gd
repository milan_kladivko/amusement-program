
#===============================================================================
#  Player's camera
# 
#  Responds to `CamArea` areas that change where the camera should point.
# 
#  Contains easing so we can interpolate between cam-area-controlled
#  focusing and player focus.
# 
#  Also sways a little when the character moves around to sell the movement
#  better. 
#  
# 
#===============================================================================


extends Camera2D


#-------------------------------------------------------------------------------
#  Debugging label
onready var debug = $debug


#-------------------------------------------------------------------------------
#  Screenshakes
#

var screen_shaker = Screenshaker.new( self )

class Screenshaker: 
	
	# @todo/ make screenshake strength editable in settings
	var base_strength = 1.0  
	
	# Array for multiple shakes at once
	var _shake_arr = [] 
	var _cam = null
	
	
	func _init( camera: Camera2D ): 
		# Can't use `self` in a class scope so pass it 
		# into the constructor.
		_cam = camera
	
		
	func start_new( max_offset:int, len_secs:float ): 
		# Add a new screenshake event
		_shake_arr.push_front( {
			'target': len_secs, 
			'max_offset': max_offset,
			'elapsed': 0.0
		} )
				
	
	func new_frame( secs_since_last_shake:float ): 
		# Make a new screenshake offset for this frame
		# and advance all event counters by the frame delta. 
		
		# Combine all screenshake offsets into one
		var combined_offset = Vector2.ZERO
		
		for sh in _shake_arr: 
		
			# Remove all elapsed shake events
			if sh['elapsed'] > sh['target']: 
				_shake_arr.erase( sh )
			
			# Advance timer
			sh['elapsed'] += secs_since_last_shake
			
			# Make new offset -> shake
			
			# @todo/ 
			#  Pick a point along a circle that shrinks with time
			#  to have a more controlled and gradual shake. 
			
			var factor :float = (1.0 - (sh['elapsed'] / sh['target']))
			var offset_base :Vector2 = Rand.offset_radius( sh['max_offset'] )
			combined_offset += (factor * offset_base) * base_strength
		
		# Keep the offset in integers, don't want to make
		# funky floated pixel stuff and texture-tearing happen. 
		_cam.offset = combined_offset.round()

	


#-------------------------------------------------------------------------------
#  Camera offset when moving

# Movement target
onready var player : Player = get_parent().get_parent()
# Are we focusing player or are we in a cam-area?
var following_player :bool = true

# Offset from player's true position affected by player movement
var move_pos := Vector2.ZERO
# Easing when moving
var MOVE_POS_SPEED :float = 0.15

# Max offset that can be achieved by moving (might not be actual max val)
onready var VIEWPORT_WIDTH : int = get_viewport().size.x
onready var MAX_OFFSET : int = ceil(0.06 * VIEWPORT_WIDTH)


#-------------------------------------------------------------------------------
#  CamArea offsetting

var area_interp :Interp = Interp.new()

# How long do we interpolate offseting the camera
# when entering or leaving a cam-area (secs)
const AREA_INTERP_SECS :float = 1.0  

# List of areas that the player is in
var cam_areas = []


#-------------------------------------------------------------------------------
#  Utilities
#

static func _pos(node:Node2D) -> Vector2: 
	# Simple shorthand
	return node.global_position

class Interp:
	# Interpolation class that lets me interpolate between two vectors 
	# using bezier curves. 
	
	var from = Vector2.ZERO
	var to = Vector2.ZERO 
	
	var amount = 0.0   # The interpolation factor
	var length_secs = AREA_INTERP_SECS
	
	# The points for the bezier interpolation always stay the same
	var bezier_args = [
		Vector2(0.00, 0.00), 
		Vector2(0.22, 0.61), 
		Vector2(0.36, 1.00), 
		Vector2(1.00, 1.00)
	]
	
	func eval() -> Vector2: 
		# Calculate the interpolated vector at this time
		var bezier :Vector2 = _cubic_bezier( bezier_args, amount )
		var easing :float = bezier.y
		return from + ((to - from) * easing)
	
	func step(delta_secs): 
		# Advance the interpolation factor by number of seconds
		if amount < 1.0: 
			amount += (delta_secs / AREA_INTERP_SECS)
		amount = min( 1.0, amount )  
		
	func change(from:Vector2, to:Vector2): 
		# Change the values between which we're interpolating
		self.from = from
		self.to = to
		amount = 0
			
	func _cubic_bezier(p:Array, t: float) -> Vector2:
		# A formula for the cubic-bezier interpolation
		var q0 = p[0].linear_interpolate(p[1], t)
		var q1 = p[1].linear_interpolate(p[2], t)
		var q2 = p[2].linear_interpolate(p[3], t)
		
		var r0 = q0.linear_interpolate(q1, t)
		var r1 = q1.linear_interpolate(q2, t)
		
		var s = r0.linear_interpolate(r1, t)
		return s



	

#-------------------------------------------------------------------------------
#  Changing cam-area states  (left/entered)
#

func _get_latest_camArea() -> Area2D: 
	return cam_areas[0]


func add_entered_camArea(cam_area) -> void: 
	if (not cam_areas.has(cam_area)): 
		cam_areas.push_front(cam_area)
		following_player = false
		
		var from = area_interp.eval()
		var to = _pos(cam_area) - move_pos
		area_interp.change( from, to )
	
func remove_left_camArea(cam_area):
	while cam_areas.has(cam_area):  
		# remove all copies of this camArea
		var index = cam_areas.find(cam_area) 
		cam_areas.remove(index)
	following_player = (len(cam_areas) == 0)
	
	if following_player: 
		var from = area_interp.eval()
		var to = Vector2.ZERO
		area_interp.change( from, to )
		
	else:
		var from = area_interp.eval()
		var to = _pos(_get_latest_camArea()) - move_pos
		area_interp.change( from, to )
		
		
#-------------------------------------------------------------------------------
#  Frame loop
# 

func _physics_process(delta):
	
	# Advance the screen shake timers  
	# !important
	screen_shaker.new_frame( delta )
	
	# update the offsets
	var player_pos = player.global_position
	move_pos = move_pos.linear_interpolate( player_pos, MOVE_POS_SPEED )
	area_interp.step(delta)
		
	if not following_player: 
		area_interp.to = _pos(_get_latest_camArea()) - move_pos
	
	# Set the camera position
	self.position = move_pos + area_interp.eval()
	


# @UNUSED/  
#  We won't allow resizing for now, but something similiar
#  should be used in the future in addition to black bars
#  around the screen and setting the render-area properly. 
func scale_render_to_whole_multiple(): 
	""" Scale the camera as inverse to the viewport (base) resize """
	
	var base_size = __perf_render_scale.base_size
	# Get the inverse scale factor
	var to_window_scale = OS.window_size / base_size
	# Get the one that fits to the window (inscribed square)
	var axis_scale = min(to_window_scale.x, to_window_scale.y)
	# Maintain whole multiples of the base size,
	# i.e. leave the float point, divide by the render multiple
	# and add a one
	var final_scale = (fmod(axis_scale, 1) / int(axis_scale)) + 1
	# Set as the camera's scale
	zoom = Vector2(final_scale, final_scale)
