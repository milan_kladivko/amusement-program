extends Node2D

# @todo/  onready var walls_lr : Node2D = $Wall_Jumping

#
#  Manage the player's wall colliders because we can't reparent 
#  them to this Node. It's either this or not using KinematicBody2D
#  as player's root node (wouldn't be practical). 
#

var player = null
var physics_standing : CollisionPolygon2D = null
var physics_sliding  : CollisionPolygon2D = null

func init(): 
	self.player = __.get_global_player()
	physics_standing = player.get_node('Walls_Hitbox')
	physics_sliding  = player.get_node('Slide_Walls_Hitbox')


# 
#  Changing hitboxes as player changes positions / states
# 
	
onready var damage_container : Area2D = $Damage_Detector
onready var damage_standing : CollisionPolygon2D = $Damage_Detector/Standing
onready var damage_sliding : CollisionPolygon2D = $Damage_Detector/Sliding

func enable_slide(): 
	__.on(   physics_sliding )
	__.on(    damage_sliding )
	__.off( physics_standing )
	__.off(  damage_standing )
	
func enable_stand(): 	
	__.on( physics_standing )
	__.on(  damage_standing )
	__.off( physics_sliding )
	__.off(  damage_sliding )	

	
	
# 
#  Extra floor detection for allowing thin platforms
#  and better control over what 'being on the floor' even means. 
#
onready var floor_detector : Area2D = $Floor_Detector

func is_on_floor() -> bool:
	var overlaps = len( floor_detector.get_overlapping_bodies() )
	return ( bool(overlaps) )  # int>bool (0=false else=true)	

func ignore_thin_platforms_if( condition_value:bool ): 
	
	var cond_fail: bool = !condition_value
	var bit: int = __.LAYER_THROUGH_UP  # == thin platform layer
	
	player.set_collision_mask_bit( 
		bit, cond_fail )
	floor_detector.set_collision_mask_bit( 
		bit, cond_fail )	
	

# 
#  Detection for when we need to stand up after sliding. 
# 
onready var can_stand : Area2D = $Standup_Detector
func can_stand_up(): 
	# Test if player can stand up by testing the approximate region
	# around the standing hitbox. 
	#   @note; 
	#   The area is cropped around the floor to allow slight physics 
	#   errors that trigger the hitbox against the ground (might be 
	#   caused by gravity pull?). 
	
	var collision_count = len( can_stand.get_overlapping_bodies() )
	var doesnt_collide = (0 == collision_count)
	return doesnt_collide
