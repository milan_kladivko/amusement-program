extents Node

var FULL_HEALTH :int = 5
var health_points := FULL_HEALTH  # Character's amount of health points

var current_attack : String = ''
var current_attack_remaining : float = -1

var is_immune_to_damage :bool = false
var DAMAGE_IMMUNE_DURATION :float = 0.5

onready var Bar : Node2D = get_parent().hud.get_node('Hp_Bar')


func update_health(change_by:int=0) -> void: 
	
	Console.warn( 'HP ' + str(change_by) )
	
	for i in [1,2,3,4,5]: 
		var peg :Sprite = healthbar.get_node( str(i) )
		var anim :AnimationPlayer = peg.get_node('AnimationPlayer')
		
		var is_on :bool = (i <= health_points)
		var is_off :bool = (i > health_points)
				
		if is_on: 
			anim.play('idle')
		elif is_off:
			anim.play('remove')
		
	health_points += change_by
	
	Console.warn( ' > ' + str(health_points) )

	
func heal_to_full() -> void: 
	var added_health :int = FULL_HEALTH - health_points
	update_healthbar( +added_health )
	

func _init_healthbar_graphics(): 
	
	var meta_filepath := 'res://graphix/UI/healthbar.json'
	var JSON := Animation_Utils.AsepriteJSON
	var sprites := JSON.to_animated_sprites( meta_filepath )
	for peg in sprites: 
		Bar.add_child( peg )






