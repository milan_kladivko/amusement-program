extends Node

#===============================================================================
#  Randomization utilities
#===============================================================================

func range(min_, max_) -> float: 
	return rand_range(min_, max_)

func range_centered(extent:float) -> float:
	""" Shorthand for symmetric random ranges """
	return rand_range(-extent, +extent)
	

func item(arr: Array): 
	""" Chooses a random item from the array """
	var index := int( rand_range(0, len(arr)) )
	return arr[index]


func offset_extents(extents:Vector2) -> Vector2: 
	var x :float = rand_range( -extents.x, +extents.x )
	var y :float = rand_range( -extents.y, +extents.y )
	return Vector2(x, y)


func offset_radius(max_radius:float) -> Vector2: 
	var rand_offset := Vector2(max_radius, 0)  \
					  .rotated(rand_range(0, 2*PI))
	return rand_offset
