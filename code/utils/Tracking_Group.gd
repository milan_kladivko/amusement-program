extends Node

#===============================================================================
#  Group of value-tracking booleans
#  
#  @todo/ add description of tracking group
#   	
	
var dict = {}  # type: <bool_name(string), Adv_Bool>

func _init(detection_variables): 
	# Pass in a list of variable names that you want to use 
	for variable in detection_variables:
		dict[variable] = Adv_Bool.new()
	
func step_all_by(delta_secs): 
	for v in dict.values(): 
		v.step_by( delta_secs )

func _get_bool(bool_name:String) -> Adv_Bool: 
	# Get the value of a bool + check if exists in group
	if not dict.has( bool_name ): 
		print("Error: Couldn't find '%s'" % bool_name)
		assert(false)
	return dict[bool_name]
		

#-------------------------------------------------------------------------------
#  Checking functions
#
#  @note/  
#    Underscore at the end is there just to avoid lang's basic keywords, 
#    don't mistake for underscore suffixes marking private functions. 
# 

func switch_(bool_name:String, value:bool, force_change:=''): 
	var b = _get_bool(bool_name)
	b.set_and_record_change_to( value )
	
	# @refactor/  move the following to the Adv_Bool class
	if not force_change.empty(): 
		b._changed_this_frame = true
		if value == true: 
			b._changed_to_true = true
		else: 
			b._changed_to_false = true

			
func is_(bool_name:String) -> bool: 
	return _get_bool(bool_name)._val

func is_not_(bool_name:String) -> bool: 
	return not(is_(bool_name))

func started_(bool_name:String) -> bool: 
	return _get_bool(bool_name)._changed_to_true

func stopped_(bool_name:String) -> bool: 
	return _get_bool(bool_name)._changed_to_false
	
func now_changed_(bool_name:String) -> bool: 
	return _get_bool(bool_name)._changed_this_frame

func secs_since_changed_(bool_name:String) -> float: 
	return _get_bool(bool_name)._secs_since_changed

		
		
#-------------------------------------------------------------------------------
#  Type of the single tracking boolean.
# 

class Adv_Bool: 
	
	#
	#  Advanced boolean type that knows 
	#  when its value was changed. 
	#
	
	var _val := false
	var _changed_this_frame := false
	var _changed_to_true := false
	var _changed_to_false := false
	var _secs_since_changed := 0.0
	
	func set_and_record_change_to(value): 
		if _val != value: 
			_val = value
			
			_changed_this_frame = true
			_secs_since_changed = 0.0
			if value == true: 
				_changed_to_true = true
			else: 
				_changed_to_false = true
				
			
	func step_by(delta_secs): 
		_secs_since_changed += delta_secs
		# reset the recording flags
		_changed_to_true = false
		_changed_to_false = false
		_changed_this_frame = false  
