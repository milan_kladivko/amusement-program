
extends Node

# @note; EXPORTING SPREADSHEETS
#   Command for exporting correctly formatted spreadsheats and JSON files 
#   is in the project's `README.md` file. 


# @bug; @aseprite TRIMMED PACKED TEXTURES
#   
#   https://community.aseprite.org/t/4239
#   There's a bug where trimmed empty frames aren't exported in JSON meta
#   thus we can't get animation duration info for empty frames.
#   
#   For now, we're not going to mess around with individual durations
#   and we'll use the same 100ms duration for every frame + adding 
#   animation speed factor into the tag name to speed up everything. 


# @note; @performance; 
#   Loading the spritesheets dynamically cannot be done with `preload()`, 
#   so there'll always be extra time outside of the startup load
#   where the game will be reading and loading the textures and their meta.
#   Partly good, because we'll want to have between-level-loading, 
#   but it's a bit more troublesome to get going. 
	



class AsepriteJSON: 
	
	static func to_animated_sprites( json_path :String ) -> Array: 
	
		var data = __.parse_json_file( json_path )
		var spritesheet = AsepriteJSON.load_spritesheet( data, json_path )
		var sprites = AsepriteJSON._generate_animation_sprites( 
			data, spritesheet )
		
		return sprites

	
	static func _generate_animation_sprites( ase_data:Dictionary, 
											 ase_sheet:Texture ) -> Array: 
		var sprite_list := []
		
		var layer_list := AsepriteJSON.to_better_datastruct( ase_data, 
															 ase_sheet )
		for L in layer_list: 
			# Console.log( '\n' )
			# Console.log( debug_layer( L ) )
		
			var sprite := Sprite.new()
			sprite.set_name( L.name )
			var player := AnimationPlayer.new()
			sprite.set_texture( L.image )
			sprite.region_enabled = true  # Frame has regions
			sprite.centered = true  # Offsets are calced from the center
		
			sprite_list.push_front( sprite )
		
			player.name = 'AnimationPlayer'
			sprite.add_child( player )
		
			for T in L.tag_list: 
			
				var anim := Animation.new()
				player.add_animation( T.name, anim )
		
				anim.set_loop( T.loop )
				
				var index_offset :int = anim.get_track_count()
				
				anim.add_track( Animation.TYPE_VALUE, index_offset )
				anim.track_set_path( index_offset, '.:offset' )
				anim.value_track_set_update_mode( 
					index_offset, Animation.UPDATE_DISCRETE )
			
				var index_region :int = anim.get_track_count() 
				anim.add_track( Animation.TYPE_VALUE, index_region )
				anim.track_set_path( index_region, '.:region_rect' )
				anim.value_track_set_update_mode( 
					index_region, Animation.UPDATE_DISCRETE )
			
				# @todo; When trimmed, add empty frames?? 
				var elapsed_secs :float = 0
				for F in T.frame_list: 
					anim.track_insert_key( index_region, 
										   elapsed_secs, F.region ) 
					anim.track_insert_key( index_offset, 
										   elapsed_secs, F.offset )
					elapsed_secs += F.duration_secs / 3
			
				anim.length = elapsed_secs 
		
		return sprite_list

		
	
	static func load_spritesheet( aseprite_data :Dictionary, 
								  json_path :String ) -> Texture: 
		var folder = json_path.get_base_dir()
		var relpath = aseprite_data.meta.image
		var abspath = folder.plus_file( relpath )
		
		var sheet: Texture = load( abspath )
		return sheet
	
	
	
	static func to_better_datastruct( aseprite_data :Dictionary, 
									  texture :Texture ) -> Array: 
		var layer_list :Array = []

		for it in aseprite_data.meta.layers: 
			var L := Layer.new()
			L.name = it.name
			L.image = texture 
			L.tag_list = []
			layer_list.push_front( L )
	
			var meta :Dictionary = aseprite_data.meta
			for i in len(meta.frameTags) / len( meta.layers ): 
				var tag_data = aseprite_data.meta.frameTags[ i ] 
				
				var tag := Tag.new()
				tag.frame_range = _Range.new( tag_data.from, tag_data.to )
				
				# In-tagname parameters (write directly into aseprite tags)
				var comps :Array = tag_data.name.split(':')
				tag.name = comps[0]
				
				tag.loop = false
				tag.speed = 1.0 
				for it in comps: 
					if it == '@': 
						tag.loop = true
					var duration_scale :float = float( it )
					if duration_scale > 0: 
						tag.speed = 1 / duration_scale
				
				tag.frame_list = []
				for i in (tag_data.to - tag_data.from + 1): 
					var F := Frame.new()
					tag.frame_list.insert( i, F )
					
					# @warning; modifying ms in ase is not supported! 
					var FIXED_ASE_DURATION_SECS :float = 0.1
					F.duration_secs = tag.speed * FIXED_ASE_DURATION_SECS
					F.region = Rect2( 0,0,0,0 )
	
				L.tag_list.push_front( tag )  # order doesn't matter
			# assert( false )
			
		for key in aseprite_data.frames.keys(): 
			
			# What layer does the frame belong to? 
			var layer_name :String = __.substr_between('(', ')', key)
			var L :Layer = null  
			for listed_layer in layer_list: 
				if listed_layer.name == layer_name: 
					L = listed_layer
					
			# Where have we seen this frame? In which tag? 
			var F :Frame = null
			var T :Tag = null
			var ase_index :int = int(__.substr_between(') ', '.ase', key))
			for tag in L.tag_list: 
				if tag.frame_range.includes( ase_index ): 
					var frame_index :int = ase_index - tag.frame_range.from
					F = tag.frame_list[ frame_index ]
					T = tag
					
			# Frame isn't tagged, it doesn't belong to an animation
			if F == null: 
				Console.err( 'untagged frame ' + key + '\n' )
				
			else: 
				# Set animation data (not duration_secs, see top of file)
				var value :Dictionary = aseprite_data.frames[ key ]
				var freg :Dictionary = value['frame']  # frame region data
				F.region = Rect2( freg.x, freg.y, freg.w, freg.h )
				var sprite_center := 0.5 * Vector2( 
					value['sourceSize'].w, 
					value['sourceSize'].h) 
				var trim_center := 0.5 * Vector2( 
					value['spriteSourceSize'].w,
					value['spriteSourceSize'].h)		
				var topleft_offset := Vector2( 
					value['spriteSourceSize'].x, 
					value['spriteSourceSize'].y)
				F.offset = (topleft_offset + trim_center) - sprite_center 
			
	
		return layer_list	


class Layer: 
	var name :String
	var image :Texture
	var tag_list :Array		
	
class Tag: 
	var name :String
	var frame_range :_Range
	var loop :bool
	var speed :float
	var frame_list :Array = []
	
class Frame: 
	var duration_secs :float
	var region :Rect2
	var offset :Vector2
	
class _Range:
	var from :int
	var to   :int
	func _init( from: int, to: int ): 
		self.from = from
		self.to   = to
	func includes( value: int ) -> bool: 
		return bool( value >= from and value <= to )	

	
func debug_layer( L: Layer ) -> String: 
	var s = '%s: ' % name
	for tag in L.tag_list: 
		s += '\n  name:%s i:<%d,%d> ($=%s):' % [
			tag.name, 
			tag.frame_range.from, tag.frame_range.to, 
			tag.loop ]
		for frame in tag.frame_list: 
			# @todo; duration is now float!! 
			s += '\n    ms:%d | rect%s' % [ 
				frame.duration_secs, frame.region ]
	return s

