
#===============================================================================
#  The developer console 
#  ...so we don't have to have 12 million function keys
#===============================================================================

extends Node


#  Opening the console  --------------------------------------------------------

var console : Panel = null
var output : RichTextLabel = null
var cmd_line : TextEdit = null

var is_shown : bool = false

func _ready(): 
	
	# Don't run on F6'ing we're gonna have to find 
	# the dev console node, of course
	if __.f6_mode():
		return
	
	console  = __.ROOT_NODE.find_node('Dev_Console', true, false) 
	if !console: 
		print("console node not found")
	output   = console.find_node('Output')
	cmd_line = console.find_node('Command Line')
	
	toggle_console(false)  # ensure the console is closed on startup
	output.bbcode_enabled = true  # don't use .text directly, this can format
	
	# help should always exist as the first command! 
	register_command(
		'help', 
		'Prints all commands and their descriptions', 
		[], self, 'print_help')
	# exit is not needed, but nice to have
	register_command(
		'exit', 
		'Quits the game', 
		[], self, 'exit_game')
	

func _input(event): 
	if event is InputEventKey and event.pressed: 
		if event.scancode == KEY_QUOTELEFT or \
		   event.scancode == KEY_SEMICOLON: 
			
			toggle_console()
			
			# don't write the key used to open the console
			get_tree().set_input_as_handled()  

	
		if event.scancode == KEY_ENTER:
			if not event.get_shift():  # let shift+enter do enter
				
				exec_commands_in_text( cmd_line.text )
				cmd_line.text = ''
				
				# don't write the enter key
				get_tree().set_input_as_handled()


func toggle_console( force_state=null ) -> void: 
	
	is_shown = not is_shown
	if force_state != null: 
		is_shown = force_state
		
	if is_shown: 
		cmd_line.grab_focus()
	console.visible = is_shown


	
	
#  Command Mechanics  ----------------------------------------------------------

class Command: 
	
	var command_name: String
	var description: String
	var arguments: Array
	var invoker: Node
	var function_name: String
	
	func _init(command_name: String,
			   description: String, 
			   arguments: Array, 
			   invoker: Node, 
			   function_name: String): 
		self.command_name = command_name
		self.description = description
		self.arguments = Arg.parse_list_of( arguments )
		self.invoker = invoker
		self.function_name = function_name
	
	func to_string() -> String: 
		var st = self.command_name
		if len(self.arguments) == 0:
			st += '( )'
		else:
			st += '( '
			for arg in self.arguments:
				st += (arg.type + ' ')
			st += ')'
		return st
			
	

class Arg: 
	var type: String
	var help: String
	var types: Array = ['str', 'float', 'int', 'bool']
	func _init(type='str', help=''): 
		if not type in types: 
			print('Invalid console command argument `%s`' % type)
			assert( false )
		self.type = type
		self.help = help
		
	static func parse_list_of(arguments:Array) -> Array:
		# batch call inits to make multiple arguments
		var arg_objects :Array = []
		for args_for_init in arguments: 
			var new_arg_object = Arg.callv( 'new', args_for_init )
			arg_objects.push_front( new_arg_object )
		return arg_objects
		
	
	
var COMMAND_LIST = [] 


func register_command( command_name: String,
					   description: String, 
					   arguments: Array, 
					   invoker: Node, 
					   function_name: String ) -> bool: 
	
	var new_command := Command.new(command_name, description, 
								   arguments, invoker, 
								   function_name)
	for it in COMMAND_LIST: 
		if it.command_name == new_command.command_name: 
			self.warn('Command with the name `%s` already exists!\n'  \
					  % new_command.command_name)
			return false
		
	COMMAND_LIST.push_back( new_command )
	Console.succ('+command: %s\n' % new_command.to_string())
	return true

	
func find_command( command_name:String ) -> Command: 
	var found: Command = null
	for it in COMMAND_LIST: 
		if it.command_name == command_name: 
			found = it
	return found


func try_to_exec_command( full_command_string: String ) -> bool: 

	# @todo/  invoke with a passed argument array
	
	var command_name = full_command_string.strip_edges()
	if command_name == '':  return false  # ignore empty commands
	
	var arglist_start_index = full_command_string.find('(')
	var has_args := bool(arglist_start_index != -1)
	if has_args: 
		command_name = command_name.substr( 0, arglist_start_index )
	
	var cmd: Command = find_command( command_name )
	if cmd == null: 
		Console.err('> %s command does not exist!\n' % command_name)
		return false
	else: 
		var args := []
		if has_args:
			var args_str :Array = __.substr_between(
				'(', ')', full_command_string).split(',')
			
			Console.log('> %s( ' % command_name)
			for i in range(len(args_str)): 
				# @todo/  Zip with type, try to parse into type
				args.push_front( args_str[i] )
				
				
				Console.log('%s ' % args_str[i])
			Console.log(')\n');
		else:
			Console.log('> %s( )\n' % command_name) 
		
		cmd.invoker.callv( cmd.function_name, args )
		return true
	

	
	



func parse_text_into_command_string_list( text: String ) -> Array: 
	text = strip_useless_chars( text )
	var command_list_with_args = split_into_commands( text )
	return command_list_with_args
	
	
func exec_commands_in_text( text: String ) -> void: 	
	var commands := parse_text_into_command_string_list( text )
	for it in commands: 
		var success:bool = try_to_exec_command( it )
	
			
			
#  Utilities for parsing the command strings  ---------------------------------

func strip_useless_chars(string:String) -> String:
	var nothing = ''
	var chars = [' ', '\t', '\n']
	for it in chars: 
		string = string.replace( it, nothing )
	return string

func split_into_commands(string:String) -> Array: 
	return Array( string.split( ';' ) )
	


#  Logging into the dev console  -----------------------------------------------

var COLOR_NORMAL = Color(1.0, 1.0, 1.0,  0.9)
var COLOR_WARN   = Color(1.0, 1.0, 0.1,  0.9)
var COLOR_SUCC   = Color(0.1, 1.0, 0.1,  0.9)
var COLOR_ERR    = Color(1.0, 0.1, 0.1,  0.9)
func _colored_output( text:String, color:Color ): 
	output.push_color( color )
	output.append_bbcode( text )
	output.pop()

	
func log( text: String ) -> void: 
	_colored_output( text, COLOR_NORMAL )
	
func warn( text: String ) -> void: 
	_colored_output( text, COLOR_WARN )
	
func err( text: String ) -> void:
	_colored_output( text, COLOR_ERR )

func succ( text: String ) -> void: 
	_colored_output( text, COLOR_SUCC )

	
func print_help(): 
	self.log('List of commands: \n')
	output.push_indent( 1 )
	for it in COMMAND_LIST: 
		self.log( it.to_string() + '\n' )
		# @todo;  Print the arguments, including the types
		output.push_indent( 1 )
		self.log( it.description + '\n' )
		output.pop()
	output.pop()
	
func exit_game(): 
	get_tree().quit()
	

