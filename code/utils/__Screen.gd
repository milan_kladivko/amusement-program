extends Node


#  Code done on program start
func _ready():
	cycle_window_size()

func _input(event): 
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_F10: 
			cycle_window_size()
	
	

#===============================================================================
#  WINDOW SCALING AND SIZING
#===============================================================================

const WINDOW_SCALE = 2
var max_scale = 3
var current_scale = max_scale
onready var base_game_resolution = get_tree().get_root().get_size()

func cycle_window_size():
	var scale = (current_scale + 1) % max_scale + 1
	var window_size = scale * base_game_resolution
	OS.set_window_size(window_size)
	
	if scale == 1: 
		# right bottom corner
		var screen_size : Vector2 = OS.get_screen_size()
		var pos = screen_size - (scale * base_game_resolution)
		pos.x -= 10
		pos.y -= 60
		OS.window_position = pos
	else: 
		OS.center_window()
	
	current_scale = scale







#===============================================================================
#  Capturing the screen I guess
#===============================================================================


class Tilemap_Screenshot extends Node: 

	var target : Node2D = null
	var cam :Camera2D = null
	var wait :float
	var SCREENSHOT_FOLDER :String = 'tilemap screenshots/'
	
	static func capture_and_save(tilemap:Node, wait:float=3.0) -> bool: 
		if tilemap.get_class().to_lower() != 'tilemap': 
			Console.err("Cannot capture a non-tilemap \n")
			return false
		
		var obj := Tilemap_Screenshot.new(tilemap, wait)
		
		var settings_ok :bool = obj._check_project_settings()
		if settings_ok: 
			obj._prepare_area()
			var filename :String = tilemap.name + '.png'
			obj._capture_and_save_as( filename )
			Console.succ("Saved a screenshot: `%s`\n" % filename)
			return true
		else: 
			var msg = 'Please change the project settings to:    \n' + \
					  '  display/window/stretch/mode => disabled \n' + \
					  '  display/window/stretch/aspect => ignore \n'
			Console.err( msg )
			return false
			
			
	
	
	func _init(target, wait:float=3.0): 
		self.target = target
		self.cam = Camera2D.new()
		self.target.add_child(cam)
		self.wait = wait
			

	func _check_project_settings() -> bool: 
		var mode = ProjectSettings.get_setting(
			'display/window/stretch/mode')
		var aspect = ProjectSettings.get_setting(
			'display/window/stretch/aspect')
		var correct_settings :bool = (mode == 'disabled' and  \
									  aspect == 'ignore')
		return correct_settings
	
	
	func _prepare_area():
		# Get area that has to be screenshotted
		var area = _get_capture_rect()

		# Center the cam on the area
		self.cam.make_current()
		var area_center :Vector2 = (area.position + area.end) / 2
		cam.global_position = area_center
		
		# Make the window as big as the area
		OS.set_window_size( area.size )
		OS.center_window()
			
		
	func _get_capture_rect() -> Rect2: 
		# Get world-coordinate rectangle for positioning
		# the camera and resizing the window. 
		var target_class = target.get_class().to_lower()
		if target_class == 'tilemap': 
			var map = (target as TileMap)
			var rect_map = map.get_used_rect()
			var a_bit = Vector2(120, 100)  # @todo; find out why tilemap's used rectangle is off by a bit 
			var rect_world := Rect2( 
				target.map_to_world( rect_map.position ), 
				target.map_to_world( rect_map.size ) + a_bit )
			return rect_world
		else: 
			return Rect2()  # @todo; what do we do for capturing something that isn't even a tilemap? 

		
	func _capture_and_save_as(filename:String): 
		var full_filepath = SCREENSHOT_FOLDER + filename
		
		# Wait before taking a screenshot
		yield(target.get_tree().create_timer(self.wait), 'timeout')
		
		var img = target.get_viewport().get_texture().get_data() 
	
		yield(target.get_tree(), 'idle_frame')  # delay func till next frame
		yield(target.get_tree(), 'idle_frame')
		
		img.flip_y()  # the screen is captured upside down
		img.save_png( full_filepath )
		









