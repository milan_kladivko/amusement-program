extends Label


var frame_times = []
var FRAMES_FOR_AVG = 4
var frame_counter = 0


var MAX_FRAMERATE = 60
var WARN_FRAMERATE = 50
var FRAMES_FOR_GRAPH = 90
var GRAPH_BG_COLOR := Color(0,0,0, 
							0.3)
var GRAPH_COLOR := Color(0.4, 0.4, 1.0, 
						 0.8)
var GRAPH_COLOR_WARN := Color(1.0, 0.4, 0.4, 
							  0.8)

func _ready(): 
	Console.register_command(
		'fps', 
		'Toggles the fps graphing and framerate draw', 
		[], self, 'toggle')


func toggle(): 
	self.visible = not self.visible
		

func _draw(): 
	if not visible:  
		return
	
	var canvas := (self as CanvasItem)
	
	# frame
	var area := Rect2( Vector2.ZERO, 
					   (self as Control).rect_size )
	canvas.draw_rect( area, GRAPH_BG_COLOR )
	
	# lines
	for i in len(frame_times): 
		var time:float = frame_times[i]
		var height:float = (rect_size.y * time / MAX_FRAMERATE)
		var offset:int = FRAMES_FOR_GRAPH - (i)
		var color := GRAPH_COLOR if time > WARN_FRAMERATE  \
					 else GRAPH_COLOR_WARN
		canvas.draw_line( 
			Vector2( offset, rect_size.y), 
			Vector2( offset, rect_size.y - height ), 
			color, 1 )


func _process(delta:float) -> void: 
	if not visible: 
		frame_times = []  # so it doesn't remember last year's frames
		return 
	
	if delta == 0.0:  
		return 
	frame_counter = (frame_counter + 1) % FRAMES_FOR_AVG

	var framerate: float = 1.0 / delta
	# var framerate: float = Performance.get_monitor( Performance.TIME_FPS )

	frame_times.push_front( framerate )
	var snip = max(FRAMES_FOR_AVG, FRAMES_FOR_GRAPH)
	while len(frame_times) > snip: 
		frame_times.pop_back() 
		
	(self as CanvasItem).update()
	
	if frame_counter == 0: 
		var sum: float = 0
		var divide_by: int = 0
		for i in min(FRAMES_FOR_AVG, len(frame_times)): 
			var time = frame_times[i]
			sum += time
			divide_by += 1
		var average: int = floor(float(sum) / divide_by)
		(self as Label).text = str(average)
	

