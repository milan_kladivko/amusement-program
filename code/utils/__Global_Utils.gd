extends Node

#
#  Layer names
# 
const LAYER_PLAYER = 0
const LAYER_ENEMIES = 1
const LAYER_WALL = 2
const LAYER_HAZARD = 4
const LAYER_PICKUP = 4
const LAYER_THROUGH_UP = 5


# 
#  Important constants 
# 
const FRAME_DURATION = 1/60.0
onready var ROOT_NODE = get_tree().get_root().get_node('game_root')
func f6_mode() -> bool:
	return self.ROOT_NODE == null


#  Physics, collisions and colliders

func get_physics_gravity(any_2d_node : Node2D) -> Vector2:
	""" 
	Read the project's global physics gravity set in the project's
	preferences.
	"""

	var DEFAULT_GODOT_GRAVITY_SCALE : float = 1.0 / 98

	var world_space = any_2d_node.get_world_2d().get_space()
	
	var vector : Vector2 = Physics2DServer.area_get_param(
		world_space, 
		Physics2DServer.AREA_PARAM_GRAVITY_VECTOR) 
	var atten : float = Physics2DServer.area_get_param(
		world_space,
		Physics2DServer.AREA_PARAM_GRAVITY_POINT_ATTENUATION)
	var scaling : float = Physics2DServer.area_get_param(
		world_space,
		Physics2DServer.AREA_PARAM_GRAVITY)
		
	return vector * atten * scaling * DEFAULT_GODOT_GRAVITY_SCALE


func collect_collisions(body : KinematicBody2D): 
	var collisions = {
		'n': body.get_slide_count(),
		'names': [], 
		'colliders': [],
		'collisions': []
	}
	for i in collisions.n:
		var collision = body.get_slide_collision(i)
		collisions.names.append(collision.collider.name)
		collisions.colliders.append(collision.collider)
		collisions.collisions.append(collision)
	return collisions


const TOGGLE_VISIBILITY = true

func on(coll : CollisionPolygon2D): 
	""" Enables a collision polygon and optionally toggles visibility """
	coll.disabled = false
	if TOGGLE_VISIBILITY: coll.visible = true	
	
func off(coll : CollisionPolygon2D): 
	""" Disables a collision polygon and optionally toggles visibility """
	coll.disabled = true
	if TOGGLE_VISIBILITY: coll.visible = false

	

# Polygons
	
func polystats_of(polygon : PoolVector2Array):
	
	var max_pos := Vector2.ZERO
	var min_pos := Vector2.ZERO
	for vertex in polygon: 
		if vertex.x > max_pos.x: 
			max_pos.x = vertex.x
		if vertex.y > max_pos.y: 
			max_pos.y = vertex.y
		if vertex.x < min_pos.x: 
			min_pos.x = vertex.x
		if vertex.y < min_pos.y: 
			min_pos.y = vertex.y
	
	var extents =  {
		'min_x': min_pos.x, 
		'max_x': max_pos.x, 
		'min_y': min_pos.y,
		'max_y': max_pos.y, 
		'width': max_pos.x - min_pos.x,
		'height': max_pos.y - min_pos.y 
	}
	return extents


#  Files
func parse_json_file( filepath:String ) -> Dictionary: 
	
	var file = File.new()
	var err = file.open( filepath, file.READ )
	if err: 
		print("Couldn't open JSON: `" + filepath + "`")
		assert( true )
		return {}
	
	var text = file.get_as_text()
	file.close()
	
	var json_parse = JSON.parse( text )
	if json_parse.error: 
		print("Couldn't parse JSON: `" + filepath + "`")
		print('  @line: '+ json_parse.error_line)
		print('  msg: '+ json_parse.error_string)
		assert( true )
		return {}
	
	var json_data:Dictionary = json_parse.result
	return json_data


#  Strings

func substr_between( delim_start:String, delim_end:String, 
					 target:String ) -> String: 
	
	var i_delim_start:int = target.find( delim_start, 0 )
	var i_delim_end:int   = target.find( delim_end, i_delim_start )
	
	if i_delim_start == -1 or i_delim_end == i_delim_start: 
		print("Couldn't find text between `%s` and `%s` \nin string `%s`"  \
			  % [delim_start, delim_end, target])
		assert( true )
	
	var found_len:int   = i_delim_end - i_delim_start - 1
	var found_start:int = i_delim_start + 1
	
	var found:String = target.substr( found_start, found_len )
	return found


#  Node manipulations

# @bug; Can't use the `Player` type here, godot freaks out
#   and reports a completely unrelated false error elsewhere
#   down the line. This might be because of some type load
#   when using it in an Autoload script (type wasn't found yet). 
#   Moral of the story: don't use `class_name` directives yet, 
#   they are very unpredictable in many cases right now. 
var PLAYER :Node2D = null
func register_player_globally( player_node: Node2D ) -> void: 
	PLAYER = player_node	
func get_global_player() -> Node2D: 
	return PLAYER
	

func find_first_scripted_ancestor_node( start:Node ) -> Node:
	if start.get_script() != null: 
		return start
	var check :Node = start;
	while check.get_script() == null: 
		check = check.get_parent() 
		if check == ROOT_NODE:
			return null
		if check.get_script() != null: 
			return check
	return null  # for F6ing where we may not have our root
		
		
	





