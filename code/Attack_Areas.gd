extends Node2D

class_name Attack_Areas

var FRAME = 1/60.0

var ACTIVE_TIME : float = FRAME * 5
var active_areas := []

onready var attacker : Node2D = get_parent()

	
func switch_collider(area : Area2D, enable : bool): 
	area.get_children()[0].disabled = not enable

	
func _process(delta):
	
	for area_dict in active_areas:
		 
		var a : Area2D = area_dict.area2d
		a.visible = true
		
		if area_dict.remaining - delta > 0:
			area_dict.remaining -= delta
			 
			for enemy in a.get_overlapping_bodies(): 
				print(enemy.name + ' hit by ' + a.name)
				# Trigger the enemy's get_hit() here
				# ...or use a signal i guess??
				
				enemy.get_hit(attacker)
		
		
		else: 
			a.visible = false
			switch_collider(a, false)
			active_areas.erase(area_dict)



func enable_attack_area(area : Area2D, duration := ACTIVE_TIME):
	
	# Add to the detection list (i don't like signals)
	active_areas.append({
		'area2d': area, 
		'remaining': duration
	})
	# Enable the collision (turn off after `duration' expired, --hopefully--)
	switch_collider(area, true)
	
