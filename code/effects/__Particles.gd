
#===============================================================================
#  Particle singleton; globally autoloaded script. 
#===============================================================================


# This has to be a node, we need to have child nodes 
# with absolute position
extends Node 
onready var ROOT_NODE = get_tree().get_root().get_node('game_root')
var particle_parent :Node = null

# Animation name used in the generated AnimationPlayer 
const PARTICLE_ANIMATION_NAME = 'play'



func _ready(): 
	# Read the list of particles and their metadata and prepare
	# template nodes to be copied as visible effects at runtime. 
	
	# Don't init particles if we're F6'ing
	if ROOT_NODE == null:   return 
	
	# make a parenting node for holding particles near the root node
	particle_parent = Node.new()
	particle_parent.set_name('Particle_Holder')
	ROOT_NODE.add_child(particle_parent)

	# read metadata and create particle animation node templates	
	for image_meta in particle_metadata: 
		__image_meta_to_particle(image_meta)
	print('[particles loaded]')

	

func play_particle(particle:Sprite) -> Sprite: 
	""" 
	Play a particle created with `spawn_new_particle()' 
	""" 
	
	var player = (particle as Sprite).get_node('player')
	(player as AnimationPlayer).play(PARTICLE_ANIMATION_NAME)
	return particle



func spawn_new_particle(name:String, bind_to:Node=null) -> Sprite:
	"""
	Spawns a new particle by copying a premade node so we can make 
	multiple independent particles at once, remove them whenever 
	we want and there's only copying at the time of particle spawning.
	"""
	
	var template = particle_parent.get_node(name)  # get template
	var particle = (template as Sprite).duplicate(0)  # and copy
	
	if bind_to == null: 
		particle_parent.add_child(particle)
	else: 
		bind_to.add_child(particle)
	
	return particle



func collect_matching_particle_names(wildcard_name:String) -> Array:
	""" Makes an array of strings matching the wildcard string """ 
	var collected_matches = []
	for particle_node in particle_parent.get_children(): 
		if particle_node.name.match( wildcard_name ): 
			collected_matches.append( particle_node.name )
	return collected_matches



# Particle types, data paths and their playback metadata
var particle_metadata = [
	{
		'texture': preload('res://graphix/particles/hits.png'), 
		'hframes': 30, 
		'vframes': 3, 
		'parts': {
			'get_slashed': {
				# 'line': 0, 
				# 'frame_len': 0.04
			}, 
			'get_pierced': {}, 
			'get_shot': {}
		}
	}, {
		'texture': preload('res://graphix/particles/steps.png'), 
		'hframes': 32, 
		'vframes': 512 / 32, # could be simplified away if image meta was read
		'parts': {
			# straight up
			'simple_0': {},
			'simple_1': {},
			'simple_2': {},
			'simple_3': {},
			'run_small_0': {},
			'run_small_1': {},
			'run_small_2': {},
			'run_small_3': {},
			'run_big_4': {},
			'run_big_5': {},
			'run_big_6': {},
		}
	}
]



func __image_meta_to_particle(image_meta:Dictionary) -> void: 
	"""
	Preloads a particle type by creating a template of the final effect
	nodes. These nodes are then copied from this template, moved into the 
	desired position and played using an animation node.

	Although this is done with performance in mind, I have no idea
	if this helps at all or if it is the best approach. 
	Only thing I know is that the built-in particles are not enough 
	for almost anything complex (without huge fiddling).
	"""
	
	var mt = image_meta  # shorthand
	
	var DEFAULT_FRAMELEN = 0.04
	var PLAYER_NODE_NAME = 'player'
	var SPRITE_ZINDEX = +10

	# Go through the various file's particles
	var particle_names = mt['parts'].keys()
	var particle_data = mt['parts'].values()
	for i in len(mt['parts']): 
		
		# Make the nodes
		var sp := Sprite.new()
		sp.name = particle_names[i]
		particle_parent.add_child(sp)
		
		var player := AnimationPlayer.new()
		player.name = PLAYER_NODE_NAME
		sp.add_child(player)
		
		# Apply metadata and other sprite props
		sp.z_index = SPRITE_ZINDEX
		sp.visible = false  # only a template, enable this when animating
		sp.texture = mt['texture']
		sp.hframes = mt['hframes']
		sp.vframes = mt['vframes']
		
		# Make the frame iteration animation 
		var anim := Animation.new()
		player.add_animation(PARTICLE_ANIMATION_NAME, anim)
		var track_i = 0
		anim.add_track(Animation.TYPE_VALUE, track_i)
		anim.track_set_path(track_i, '.:frame')
		
		# Plop down the endpoints, linearly interpolate in between
		anim.track_set_interpolation_type(
			track_i, Animation.INTERPOLATION_LINEAR)
		anim.value_track_set_update_mode(
			track_i, Animation.UPDATE_CONTINUOUS)
		
		# Which frame values to use
		var line :int = i
		if particle_data[i].has('line'): 
			line = particle_data[i]['line']
		var hframes :int = mt['hframes']
		var frame_start :int = line * hframes
		var frame_end :int = frame_start + hframes
		var length :float = DEFAULT_FRAMELEN * hframes
		if particle_data[i].has('frame_len'): 
			length = particle_data[i]['frame_len'] * hframes
		
		# Apply start and end keyframes
		anim.track_insert_key(track_i, 
							  0.0, frame_start)
		anim.track_insert_key(track_i, 
							  length, frame_end)
		anim.length = length  # Important!!
		
		# Enable visible on animation start
		track_i += 1
		anim.add_track(Animation.TYPE_VALUE, track_i)
		anim.track_set_path(track_i, '.:visible')
		anim.track_insert_key(track_i, 0.0, true)
		# anim.track_insert_key(track_i, length, false)
		
		# Destroy particle on animation end
		anim.add_track(Animation.TYPE_METHOD, track_i)
		anim.track_set_path(track_i, '.')
		anim.track_insert_key(
			track_i, length, {'method': 'queue_free', 'args': []})
		
