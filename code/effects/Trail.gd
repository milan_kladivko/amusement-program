
#===============================================================================
#  Trail node
# 
#  Records its previous global positions and connects them up with a line, 
#  making a trail effect. 
#
#===============================================================================

extends Node2D

#-------------------------------------------------------------------------------
#  Fields
#

export(int) var MAX_FRAMES = 20
export(bool) var ANTIALIAS = true
# export(int) var MAX_DISTANCE

var THICKNESS :float = 1.0
var COLOR     := Color(1,0,0, 1)
var COLOR_END := Color(1,0,0, 0) 

# export a color
# export a thickness

# Position array
# @note/ newest positions to the front, trim the back
var _pos_arr := []


#-------------------------------------------------------------------------------
#  Constructor
#
func _ready():
	pass

#-------------------------------------------------------------------------------
#  Drawing the trail
#
func _draw(): 
	var current_pos :Vector2 = self.global_position
	var THE_LAST_ONE = 1
	
	for i in len( _pos_arr ) - THE_LAST_ONE: 
		
		var start :Vector2 = _pos_arr[i] - current_pos
		var end   :Vector2 = _pos_arr[i + 1] - current_pos

		var blend :float = ( float(i) / MAX_FRAMES )
		var segment_color := COLOR.linear_interpolate(COLOR_END, blend)

		self.draw_line( start, end, segment_color, THICKNESS )


#-------------------------------------------------------------------------------
#  Every-frame actions
#
func _process(delta:float):
	
	# Record the position 
	_pos_arr.push_front( self.global_position )
	
	# Trim the positions to a number
	while len(_pos_arr) > MAX_FRAMES: 
		_pos_arr.pop_back()
	
	# Trigger redraw every frame
	self.update()  







