extends Area2D
class_name Checkpoint


onready var anim : AnimationPlayer = $AnimationPlayer
onready var anim_timer : Timer = $AnimationTimer
var active


func _ready(): 
	# set position to rounded, otherwise it fucks with the cam
	position = position.round()
	
	self.set_waiting_for_player()
	
	z_index = 0  # keep behind the player, if possible



func _on_Checkpoint_body_entered(player):
	""" Set player's checkpoint to this one """ 
	
	# Disable current checkpoint
	if player.last_checkpoint != null: 
		player.last_checkpoint.set_waiting_for_player() 

	player.full_heal()
	
	# Set player's last checkpoint to this one
	self.set_current()
	player.last_checkpoint = self
	


func set_current(): 
	active = true
	change_debug_look()
	
func set_waiting_for_player():
	active = false
	change_debug_look()


	
	
func change_debug_look():
	var X_OFFSET__OFF = 32
	var X_OFFSET__ON = 48
	$Sprite.region_rect.position.y = X_OFFSET__ON if active  \
									 else X_OFFSET__OFF
	$Particles.emitting = active
