extends TileMap

#--------------------------------------------------------
# Placeholder Tilemap
#  
#  Allows me to draw almost all objects using a tilemap. 
#  Tiles with a prefix get deleted and replaced 
#  by a full node. 
#  
#  This TileMap contains sub-tilemaps that can have 
#  their own physics properties, layers and scripts. 
#  If I draw spike tiles, they get copied over 
#  to this sub-tilemap with "kill player if in hitbox" 
#  logic. 
#  
#  ...Basically I can do a lot more with a basic
#     TileMap; I can draw, copy, undo and delete
#     almost any object. 
# 
# 
#  Drawbacks (@todo;): 
#  - I can't create nodes that are different, except
#    for rotation and direction flips. I can't
#    make an arrow trap that has a delay on it for ex.
#  - I can't copy over rotation from the PH map
#    into the objects themselves.
#  - There *might* be a performance drawback? 
#    I'm not sure when the objects are created
#    and loaded so the performance should be tested
#    in the future. 
# 
#  - Detecting tilemap edits is too complex. 
#    I'd have to mark what was copied from where so I could
#    then delete the old nodes when I deleted their 
#    placoholders in the tileset. 
# 
#  


#===============================================================================
#  Exported variables; editable in the tool
#===============================================================================
export(bool) var HIDE_SPIKES = false


# Debug print what nodes have been generated
const PRINT_GENERATED_NODES = false

# Created nodes have to get a Z-index bigger
# than usual tiles
const PLACEHOLDER_NODE_ZINDEX = +1 

# Tiles get set to this value when placeholders
# get created as actual objects - making the tile
# empty
var REMOVED_CELL = -1

# What placeholders create what objects
var placeholder_to_scene : Dictionary = {
	'_checkpoint': preload(  \
		'res://nodes/mapping/Checkpoint.tscn'), 
	'_arrow trap': preload(  \
		'res://nodes/mapping/Arrow_Trap.tscn')
}

# Non-placeholder tiles get copied over to sub-tilesets
# that can then have their own physics properties
# and still function as tilesets
onready var spikes_tilemap = get_node('SPIKES')
onready var thin_tilemap = get_node('THINS')

# onready var submaps = get_children()  
# ... but what if I need other nodes! 
onready var submaps = [
	get_node('SPIKES'), 
	get_node('THINS')
]


func try_to_copy(name:String, pos:Vector2, map:TileMap) -> bool:
	
	if not name.begins_with( map.name ): 
		return false
	
	var set = map.get_tileset()
	
	for id in set.get_tiles_ids(): 
		var tile_name = set.tile_get_name(id)
		if name.ends_with( tile_name ): 
			map.set_cellv(pos, id)
			return true
		
	return false


func copy_if_submap(tile_name:String, pos:Vector2) -> bool: 
	
	var copied = false
	
	for map in submaps: 
		if try_to_copy(tile_name, pos, map): 
			copied = true

	return copied


func translate_tilemap_to_world(tile_pos:Vector2) -> Vector2: 
	
	var tile_size = get_cell_size()
	var tile_center = (tile_size / 2.0).round() 

	var world_pos = tile_pos * tile_size + tile_center
	return world_pos



func copy_tile_transform(tile_pos:Vector2, node:Node2D): 

	# Make a transformation matrix from those booleans
	# tiles give us, I can't read rotation and scale
	# directly from them. 
	
	var x_axis := Vector2(1, 0)  # first column of matrix
	var y_axis := Vector2(0, 1)  # .. 
	
	if is_cell_x_flipped( tile_pos.x, tile_pos.y ): 
		x_axis *= -1	
	if is_cell_y_flipped( tile_pos.x, tile_pos.y ): 
		y_axis *= -1
	if is_cell_transposed( tile_pos.x, tile_pos.y ): 
		var temp = x_axis
		x_axis = y_axis
		y_axis = x_axis
	
	# Use this matrix to rotate and scale the node
	var t := Transform2D( x_axis, y_axis, Vector2.ZERO) 
	node.set_transform( t )
	
	# Move it directly, I don't get matrices
	node.position = translate_tilemap_to_world( tile_pos )


func parse_tilemap_placeholders(): 
		
	for pos in get_used_cells(): 
		
		var id = get_cell( pos.x, pos.y )
		var tile_name = tile_set.tile_get_name( id )
		
		if placeholder_to_scene.has( tile_name ):
			
			var scene = placeholder_to_scene[tile_name]
			var node = scene.instance()
			
			copy_tile_transform( pos, node )
			node.z_index = PLACEHOLDER_NODE_ZINDEX
			
			self.add_child( node )
			set_cell( pos.x, pos.y, REMOVED_CELL )
		
		else: 
			var copied = copy_if_submap( tile_name, pos )
			if copied: 
				set_cell( pos.x, pos.y, REMOVED_CELL )



func _ready():

	parse_tilemap_placeholders()
	
	if PRINT_GENERATED_NODES: 
		print_tree_pretty()
		
	if HIDE_SPIKES: 
		$SPIKES.visible = false
	

func _input(event): 
	if event is InputEventKey: 
		if event.pressed and event.scancode == KEY_F12: 
			Screen.Tilemap_Screenshot.capture_and_save( self )
