extends Area2D
class_name CamArea


onready var focus : Position2D = $Cam_Center

export (String, 'scrolls_left', 'scrolls_right', 'centered') \
  var scroll_at_center = 'centered'

func _on_CamArea_body_entered(player):
	player.cam.add_entered_camArea(self)
	
func _on_CamArea_body_exited(player):
	player.cam.remove_left_camArea(self)


