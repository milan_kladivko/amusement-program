
#===============================================================================
# 
#  Dude. 
# 
#  Please rewrite this so it works for any pickup we'll have. 
# 
#  Thanks.
# 
#===============================================================================

extends Area2D

const WAIT_BEFORE_DESTRUCTION = 1.0

func _on_player_taking_Walljump_Pickup(body):
	(body as Player).get_walljump()
	$Label.text = 'GOT!'
	yield(get_tree().create_timer(WAIT_BEFORE_DESTRUCTION), "timeout")
	queue_free()
	
