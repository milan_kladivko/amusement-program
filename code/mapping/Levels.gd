extends Node2D

func _ready():
	add_dev_console_commands()
	
	
func add_dev_console_commands() -> void: 
	
	Console.register_command(
		'unload_level', 
		'Deletes the level from runtime', 
		[['str']], self, 'unload_level') 
	
	Console.register_command(
		'list_levels', 
		'Lists all current children of the `Level` node.', 
		[], self, 'list_level_components')
	
	Console.register_command(
		'save_tmap', 
		'Screenshots the tilemap and saves as `<level_name>.png`', 
		[['str']], self, 'screenshot_tilemap')
	
	
func unload_level( level_name:String ): 
	Console.err(level_name + '\n')
	for level in self.get_children():
		Console.warn(level.get_name() + '\n')
		if level.get_name() == level_name: 
			level.queue_free()
	

func list_level_components(): 
	
	for level in self.get_children(): 
		Console.log( level.get_name() +'\n' )
		
		for node in level.get_children(): 
			if node.get_class().to_lower() == 'tilemap': 
				Console.log( ' - ' + node.get_name() + '\n' )

func screenshot_tilemap( map_name:String ): 
	var found :Node = self.find_node( map_name )
	var success := Screen.Tilemap_Screenshot  \
						 .capture_and_save( found )
	if not success: 
		Console.err("Couldn't take the screenshot!\n")
		
		
