extends RigidBody2D


var ROAM_SPEED : float = 100
var ATTACK_SPEED : float = 260


onready var anim : AnimationPlayer = $AnimationPlayer
onready var player_detectors : Array = $Player_Detectors.get_children()


var facing_right := true
var block_dur : float = 0
var dying := false


func _ready(): 
	linear_damp = 0.1
	gravity_scale = 0


func get_hit(player:Player, ignore_hits_secs:float=0.5): 

	if block_dur <= 0: 
		# read that from the attack itself, or use a global dict
		var hit_str = 200 
		
		var dir = -1 if (player.sprite.flip_h) else +1
		linear_velocity.y = -hit_str * 0.8
		linear_velocity.x = dir * hit_str
		
		block_dur = ignore_hits_secs
		
		if true: # hp < 0:
			dying = true
			angular_velocity = randf() * 50 * dir
		
		


func _physics_process(delta):
	
	if block_dur > 0: 
		block_dur -= delta
	
	
	if not dying:
		
		anim.play("idle")
		# update facing
		if linear_velocity.x != 0: 
			facing_right = linear_velocity.x > 0
			$Sprite.flip_h = not facing_right
			$Player_Detectors.scale.x = sign(linear_velocity.x)
		
	elif dying:
		# anim.play('dying')
		gravity_scale = 1
		linear_damp = 0.2
		contact_monitor = true
		contacts_reported = 1
		
		var collided_with_walls = len(get_colliding_bodies()) > 0
		if collided_with_walls:
			queue_free()
