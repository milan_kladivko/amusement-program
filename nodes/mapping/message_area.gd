extends Area2D

func _ready():
	$msg.visible = false

func _on_slide_denied_body_entered(body):
	$msg.visible = true

func _on_slide_denied_body_exited(body):
	$msg.visible = false 
