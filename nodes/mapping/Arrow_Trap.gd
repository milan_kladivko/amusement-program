extends Node2D

export var arrow_speed :float = 450
export var shoot_delay_secs :float = 1.2
export var start_delay_secs :float = 0.0

var Arrow_Class = preload("res://nodes/enemies/Arrow_Projectile.tscn")
onready var shoot_timer : Timer = $Shoot_Timer
onready var start_delay : Timer = $StartDelay_Timer
onready var anim : AnimationPlayer = $AnimationPlayer
onready var arrow_startpos : Position2D = $Arrow_Startpos

func _ready():
	
	# Starts the shoot timer
	start_delay.one_shot = true
	start_delay.wait_time = start_delay_secs
	start_delay.start()
	
	# Triggers shooting at regular intervals
	shoot_timer.one_shot = false	
	shoot_timer.wait_time = shoot_delay_secs
	shoot_timer.autostart = false
	

func shoot(): 
	var arrow : RigidBody2D = Arrow_Class.instance()
	arrow.initiate( Vector2(arrow_speed, 0), arrow_startpos )
	arrow.sleeping = false
	

func _on_Shoot_Timer_timeout():
	anim.play('shoot')


func _on_StartDelay_Timer_timeout():
	shoot_timer.start()
