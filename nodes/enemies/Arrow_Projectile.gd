extends RigidBody2D

onready var anim : AnimationPlayer = $AnimationPlayer

func initiate(speed:Vector2, shooter:Node2D, bind_to:Node2D=__.ROOT_NODE): 
	
	sleeping = true
	
	gravity_scale = 0
	linear_velocity = speed.rotated(shooter.global_rotation)
	rotation = shooter.global_rotation
	position = shooter.global_position
	
	contact_monitor = true
	contacts_reported = 1
	
	$flip_group/CPUParticles2D.emitting = true
	
	bind_to.add_child(self)



func do_collision_action():
	sleeping = true
	var att_area = $flip_group/attack_area
	if att_area != null: att_area.queue_free()
	$flip_group/CPUParticles2D.emitting = false
	$flip_group/Sprite.visible = false
	anim.play('queue_free')

func _on_Arrow_Projectile_hit_wall(body):
	do_collision_action()
