extends KinematicBody2D

class_name Skelly

var ACCEL = 2
var WALK_SPEED = 30
var CHASE_SPEED = 80

onready var GLOBAL_GRAVITY = __.get_physics_gravity(self)
onready var UP_DIRECTION = GLOBAL_GRAVITY * -1

onready var sprite : Sprite = $Sprite
onready var anim : AnimationPlayer = $AnimationPlayer
onready var action_anim : AnimationPlayer = $ActionAnimations
onready var wall_hit_collision : CollisionPolygon2D = $wall_hit_collision
onready var attack_area : Area2D = $attack_area 
onready var attack_collision : CollisionPolygon2D = $attack_area/attack_collision
onready var vision : Area2D = $vision_cone
onready var help : Label = $state_help
onready var floor_detector : Area2D = $floor_detector

enum AI {
	WAITING, 
	SEEKING_PLAYER,
	CHASING_PLAYER,
	ATTACKING
}
var ai_action : int = AI.WAITING

var behavior_timer = 0
var seen_player : Player = null 
var getting_hit = false

var movement := Vector2.ZERO
var seek_dir = 0
var facing_right = true


var touching_floor = false
var just_landed = false
func is_on_floor(): 
	return len(floor_detector.get_overlapping_bodies()) != 0



func hflip(force_dir=0): 
	
	if force_dir != 0: 
		facing_right = force_dir > 0
		
	else: 
		if not getting_hit: 
			if movement.x > 0: 
				facing_right = true
			elif movement.x < 0: 
				facing_right = false
	
	var scale = 1 if facing_right else -1

	sprite.flip_h = not facing_right   # turn the sprite
	wall_hit_collision.scale.x = scale
	attack_area.scale.x = scale    # attack the other side ofc
	vision.scale.x = scale


func get_hit(attacker): 
	getting_hit = true
	$Particle_Spawner
	self.queue_free()  # run on a timer 
	
	
func attack_in_area(area_node_name : String): 
	
	var att_area : Area2D = get_node(area_node_name)
	var hit_bodies : Array = attack_area.get_overlapping_bodies()
	
	if len(hit_bodies) == 0: 
		return
	
	for player in hit_bodies: 
		player.get_hit_by(self)
		

func shove(set_velo : Vector2): 
	var dir_to_player = sign(seen_player.global_position.x - global_position.x)
	set_velo.x *= dir_to_player
	movement = set_velo
	hflip(dir_to_player)

	
func _physics_process(delta): 
	
	just_landed = false
	if is_on_floor(): 
		just_landed = touching_floor == false
		touching_floor = true
	else: 
		touching_floor = false
	
	
	if seen_player == null: 
		# try to see the player
		var bodies = vision.get_overlapping_bodies()
		if len(bodies) != 0: 
			seen_player = bodies[0]
			behavior_timer = 0
	else: 
		# turn the vision off, don't lag it up for no reason 
		vision.get_children()[0].disabled = true
	
		
	# Start chasing player
	behavior_timer -= delta
	if behavior_timer <= 0: 
		
		if seen_player == null: 
			ai_action = AI.SEEKING_PLAYER
			if seek_dir == 0: 
				seek_dir = Rand.item([-1, 1])
				anim.play('walk')
			else: 
				seek_dir = 0
				anim.play('idle')
			behavior_timer = Rand.item([1, 2])
			
		else: 
			ai_action = AI.CHASING_PLAYER
			behavior_timer = 99999  # chase indefinitely
	
			
	if ai_action == AI.SEEKING_PLAYER: 
	
		if seek_dir == 0: 
			movement.x *= 0.9
			anim.play('idle')
		else:
			anim.play('walk')
			movement.x += seek_dir * ACCEL
			movement.x = clamp(movement.x, -WALK_SPEED, +WALK_SPEED)
		
		
	if ai_action == AI.CHASING_PLAYER: 
		
		help.text = '!!'
		
		var pos_diff = seen_player.global_position.x - self.global_position.x
		var dir = sign(pos_diff)
		if abs(pos_diff) < 10: dir = 0
		
		if abs(pos_diff) < 100 and touching_floor and anim.current_animation != 'landing':
				action_anim.play('att_lunge')
				ai_action = AI.ATTACKING
		else: 
			anim.play('walk', -1, 1.6)  # @todo; have a proper chase anim
			movement.x += dir * ACCEL
			movement.x = clamp(movement.x, -CHASE_SPEED, +CHASE_SPEED)
			
			
	if ai_action == AI.ATTACKING: 
	
		movement += GLOBAL_GRAVITY * 0.6
		movement = move_and_slide(movement, UP_DIRECTION)
		
		if touching_floor: 
			var FLOOR_RESISTANCE = 0.8
			movement.x *= FLOOR_RESISTANCE
		
		if action_anim.current_animation != 'att_lunge': 
			ai_action = AI.CHASING_PLAYER
			behavior_timer = 9999
		
		if attack_collision.disabled == false: 
			attack_in_area('attack_area')
			
			
	else: 
	
		hflip()  # flip the enemy around, if needed
		
		movement += GLOBAL_GRAVITY
		movement = move_and_slide(movement, UP_DIRECTION)
		
	if just_landed: 
		anim.play('landing')
